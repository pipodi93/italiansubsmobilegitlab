package com.pipodi.italiansubsmobileclient.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.ItaSALogin;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.utils.Constants;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity {

    public static Context context;
    public static SharedPreferences preferences;
    public static Activity activity;

    private EditText userField;
    private EditText passwordField;
    private Button loginButton;
    private CheckBox checkBox;
    private ViewGroup viewGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        //OneSignal.startInit(this).init();
        context = this;
        activity = this;
        if (Connection.checkInternetConnection(getApplicationContext())) {
            setContentView(R.layout.login_activity_layout);
            this.viewGroup = (ViewGroup) ((ViewGroup) this
                    .findViewById(android.R.id.content)).getChildAt(0);
            preferences = this.getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE);
            this.userField = (EditText) findViewById(R.id.loginUser);
            this.passwordField = (EditText) findViewById(R.id.loginPassword);
            this.loginButton = (Button) findViewById(R.id.loginButton);
            this.checkBox = (CheckBox) findViewById(R.id.rememberLogin);
            getSupportActionBar().hide();
            TextView registerLink = (TextView) findViewById(R.id.registrationLink);
            registerLink.setText(Html.fromHtml(getString(R.string.register_string)));
            if (LoginVariables.logged) {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                if (preferences.getBoolean("REMEMBER", false)) {
                    if (Connection.checkInternetConnection(context)) {
                        String username = preferences.getString("USER", "");
                        String password = preferences.getString("PASS", "");
                        User.getInstance().setUserName(username);
                        User.getInstance().setUserPassword(password);
                        ItaSALogin login = new ItaSALogin(username, password,
                                preferences.getBoolean("REMEMBER", false), viewGroup, this.loginButton);
                        login.execute("");
                    } else {
                        Toast.makeText(context, "Connessione internet assente.", Toast.LENGTH_LONG).show();
                    }
                }
            }
            registerLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = "http://www.italiansubs.net/forum/index.php?action=register";
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                }
            });

            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getCurrentFocus() != null) {
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                    if (Connection.checkInternetConnection(context)) {
                        ItaSALogin login = new ItaSALogin(userField.getText().toString(), passwordField.getText().toString(), checkBox.isChecked(), viewGroup
                                , loginButton);
                        User.getInstance().setUserName(userField.getText().toString());
                        User.getInstance().setUserPassword(passwordField.getText().toString());
                        login.execute("");
                    } else {
                        Toast.makeText(context, "Connessione internet assente.", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            setContentView(R.layout.noconnection_layout);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        if (LoginVariables.isConnected) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            return true;
        }
        return false;
    }
}
