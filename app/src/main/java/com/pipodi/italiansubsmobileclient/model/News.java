package com.pipodi.italiansubsmobileclient.model;

import android.graphics.Bitmap;

import com.pipodi.italiansubsmobileclient.model.iterfaces.NewsInterface;

import java.io.Serializable;

/**
 * Created by pipod on 22/03/2016.
 */
public class News implements NewsInterface, Serializable {

    private int ID;
    private int newsID;
    private String title;
    private String episode;
    private String release_date;
    private Bitmap image;
    private boolean favourite = false;
    private String photoURL;

    public News(int id, int newsID, String title, String episode, String release,
                Bitmap image, String photoURL) {
        this.title = title;
        this.episode = episode;
        this.release_date = release;
        this.image = image;
        this.ID = id;
        this.newsID = newsID;
        this.photoURL = photoURL;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getEpisode() {
        return this.episode;
    }

    @Override
    public String getReleaseDate() {
        return this.release_date;
    }

    @Override
    public Bitmap getPhoto() {
        return this.image;
    }

    @Override
    public String toString() {
        return "[ID: " + this.ID + " ; NewsID: " + this.newsID + " ; Title: " + this.title
                + " ; Episode: " + this.episode + " ; Release: " + this.release_date;
    }

    @Override
    public int getID() {
        return this.ID;
    }

    @Override
    public int getNewsID() {
        return this.newsID;
    }

    @Override
    public boolean isFavourite() {
        return this.favourite;
    }

    @Override
    public void setIsFavourite(boolean isFavourite) {
        this.favourite = isFavourite;
    }

    @Override
    public String getPhotoURL() {
        return this.photoURL;
    }
}
