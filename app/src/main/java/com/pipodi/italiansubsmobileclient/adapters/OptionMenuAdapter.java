package com.pipodi.italiansubsmobileclient.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.activities.NotificationService;
import com.pipodi.italiansubsmobileclient.model.OptionTypes;
import com.pipodi.italiansubsmobileclient.model.iterfaces.OptionInterface;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;
import com.rey.material.widget.Switch;

import java.util.List;

/**
 * Created by Alex on 13/04/2016.
 */
public class OptionMenuAdapter extends BaseAdapter {

    private Context context;
    private List<OptionInterface> options;

    public OptionMenuAdapter(Context context, List<OptionInterface> options) {
        this.context = context;
        this.options = options;
    }

    @Override
    public int getCount() {
        return this.options.size();
    }

    @Override
    public Object getItem(int position) {
        return this.options.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OptionInterface currentOption = (OptionInterface) getItem(position);
        if (currentOption.getType() == OptionTypes.HEADER) {
            convertView = LayoutInflater.from(context).inflate(R.layout.header_list_layout, parent, false);
            TextView headerView = (TextView) convertView.findViewById(R.id.headerTitle);
            headerView.setText(currentOption.getTitle());
        } else if (currentOption.getType() == OptionTypes.DIRECTORY) {
            convertView = LayoutInflater.from(context).inflate(R.layout.directory_option_item_layout, parent, false);
            TextView titleView = (TextView) convertView.findViewById(R.id.optionTitle);
            titleView.setText(currentOption.getTitle());
            TextView descriptionView = (TextView) convertView.findViewById(R.id.optionDescription);
            descriptionView.setText(currentOption.getDescription());
        } else if (currentOption.getType().equals(OptionTypes.SWITCH)) {
            convertView = LayoutInflater.from(context).inflate(R.layout.switch_list_layout, parent, false);
            TextView optionTitle = (TextView) convertView.findViewById(R.id.optionTitle);
            optionTitle.setText(currentOption.getTitle());
            Switch aSwitch = (Switch) convertView.findViewById(R.id.optionSwitch);
            if (TempStorage.isServiceRunning) {
                aSwitch.setChecked(true);
            } else {
                aSwitch.setChecked(false);
            }
            aSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(Switch view, boolean checked) {
                    if (checked) {
                        context.startService(new Intent(context, NotificationService.class));

                        context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE)
                                .edit().putBoolean("STOPPEDBYUSER", false).commit();
                    } else {
                        context.stopService(new Intent(context, NotificationService.class));

                        context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE)
                                .edit().putBoolean("STOPPEDBYUSER", true).commit();
                    }
                }
            });
        } else {
            convertView = LayoutInflater.from(context).inflate(R.layout.option_item_layout, parent, false);
            TextView titleView = (TextView) convertView.findViewById(R.id.optionTitle);
            titleView.setText(currentOption.getTitle());
            TextView descriptionView = (TextView) convertView.findViewById(R.id.optionDescription);
            descriptionView.setText(currentOption.getDescription());
        }

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        switch (position) {
            case 0:
                return false;
            case 1:
                return false;
            case 7:
                return false;
            default:
                return super.isEnabled(position);
        }
    }


}
