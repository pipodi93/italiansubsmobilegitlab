package com.pipodi.italiansubsmobileclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.ForumConnection;
import com.pipodi.italiansubsmobileclient.model.ForumTypes;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

import static com.pipodi.italiansubsmobileclient.activities.MainActivity.forumConnection;

/**
 * Created by Alex on 13/04/2016.
 */
public class ForumFragment extends Fragment {

    private Context context;
    private Button upButton;
    private Button replyButton;
    private Button topicButton;
    private Button cpButton;
    private ProgressBar forumProgress;
    private Button nextButton;
    private Button previousButton;
    private TextView pageIndicator;
    private ListView forumList;
    private SwipeRefreshLayout refreshLayout;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View generalView = inflater.inflate(R.layout.forum_fragment_layout, container, false);
        forumList = (ListView) generalView.findViewById(R.id.forumList);
        refreshLayout = (SwipeRefreshLayout) generalView.findViewById(R.id.refreshLayout);
        forumList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (forumList == null || forumList.getChildCount() == 0) ?
                                0 : forumList.getChildAt(0).getTop();
                refreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
        TempStorage.startPageForum = 0;
        TempStorage.endPageForum = 19;
        TempStorage.totalTopicsOnThisForum = 0;
        TempStorage.totalForumPages = 0;
        TempStorage.forumHistory.clear();
        TempStorage.paramsHistory.clear();
        TempStorage.totalPageHistory.clear();
        upButton = (Button) generalView.findViewById(R.id.upButton);
        replyButton = (Button) generalView.findViewById(R.id.replyButton);
        topicButton = (Button) generalView.findViewById(R.id.topicButton);
        cpButton = (Button) generalView.findViewById(R.id.userCPButton);
        forumProgress = (ProgressBar) generalView.findViewById(R.id.forumProgress);
        nextButton = (Button) generalView.findViewById(R.id.nextForumPageButton);
        previousButton = (Button) generalView.findViewById(R.id.previousForumPageButton);
        pageIndicator = (TextView) generalView.findViewById(R.id.pageIndicator);
        forumConnection = new ForumConnection(context, forumList, null, upButton, replyButton, topicButton, cpButton,
                nextButton, previousButton, refreshLayout, ForumTypes.FORUM, forumProgress, pageIndicator);
        forumConnection.execute("");
        return generalView;
    }
}
