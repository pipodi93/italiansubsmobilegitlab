package com.pipodi.italiansubsmobileclient.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Alex on 19/12/2015.
 */
public class NotificationActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isTaskRoot()) {
            Intent startIntent = new Intent(this, LoginActivity.class);
            startActivity(startIntent);
        } else {
            Intent reorderIntent = new Intent(this, MainActivity.class);
            reorderIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            reorderIntent.putExtra("NOTIFICATION", true);
            startActivity(reorderIntent);
        }
        finish();
    }
}
