package com.pipodi.italiansubsmobileclient.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.activities.MainActivity;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.engine.ConnectionForSubtitle;
import com.pipodi.italiansubsmobileclient.model.iterfaces.SubtitleInterface;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Alex on 02/04/2016.
 */
public class SubtitlesListAdapter extends BaseAdapter {

    private Context context;
    private List<SubtitleInterface> items;
    private LayoutInflater inflater;

    public SubtitlesListAdapter(Context context, LayoutInflater inflater,
                                List<SubtitleInterface> items) {
        this.context = context;
        this.items = items;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public SubtitleInterface getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Use getView from the Item interface
        if (convertView == null) {
            convertView = inflater.inflate(
                    R.layout.sub_item_layout, null);
        }

        final SubtitleInterface sub = items.get(position);

        TextView showName = (TextView) convertView
                .findViewById(R.id.sub_showName);
        TextView episode = (TextView) convertView.findViewById(R.id.sub_epNum);
        TextView version = (TextView) convertView
                .findViewById(R.id.sub_version);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.folder);
        if (this.context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getInt("THEME", 0) == 0) {
            Picasso.with(this.context).load(R.drawable.zip).into(imageView);
        } else {
            Picasso.with(this.context).load(R.drawable.zip_dark).into(imageView);
        }
        Button downloadButton = (Button) convertView.findViewById(R.id.downloadButton);
        downloadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (LoginVariables.logged) {
                    if (ContextCompat.checkSelfPermission(MainActivity.context,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        ConnectionForSubtitle connection = new ConnectionForSubtitle(sub.getID(), getFileName(sub.getVersion(), sub.getShowName(), sub.getEpisode()));
                        connection.execute("");
                    } else {
                        if (ContextCompat.checkSelfPermission(context,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    0);
                        }
                    }
                } else {
                    Toast.makeText(context, "Devi essere loggato per scaricare i sottotitoli.", Toast.LENGTH_LONG).show();
                }
            }
        });
        showName.setText(sub.getShowName());
        episode.setText(sub.getEpisode());
        version.setText(sub.getVersion());

        return convertView;
    }

    private String getFileName(String version, String name, String episode) {
        if (version.equals("Normale")) {
            return checkForSpaces(name) + "." + checkForSpaces(episode);
        }
        return checkForSpaces(name) + "." + checkForSpaces(episode) + "." + version;
    }

    private String checkForSpaces(String str) {
        if (str.contains(" ")) {
            return str.replace(" ", ".");
        }
        return str;
    }
}
