package com.pipodi.italiansubsmobileclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ShowInterface;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pipod on 27/03/2016.
 */
public class ShowAdapter extends BaseAdapter implements Filterable {

    private List<ShowInterface> list = new ArrayList<>();
    private ArrayList<ShowInterface> tempList = new ArrayList<>();
    private Context context;

    public ShowAdapter(List<ShowInterface> list, Context context) {
        this.list = list;
        this.context = context;
        this.tempList.addAll(list);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ShowInterface item = this.list.get(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.show_item_layout, parent, false);
        TextView showName = (TextView) convertView.findViewById(R.id.showName);
        showName.setText(item.getShowName());
        ImageView showIcon = (ImageView) convertView.findViewById(R.id.favIcon);
        if (context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getInt("THEME", 0) == 0) {
            Picasso.with(context).load(R.drawable.fans).into(showIcon);
        } else {
            Picasso.with(context).load(R.drawable.fans_dark).into(showIcon);
        }
        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        list.clear();
        if (charText.length() == 0) {
            list.addAll(tempList);
        } else {
            for (ShowInterface show : tempList) {
                if (show.getShowName().toLowerCase().contains(charText)) {
                    list.add(show);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results.count == 0)
                    notifyDataSetInvalidated();
                else {
                    list = (List<ShowInterface>) results.values;
                    notifyDataSetChanged();
                }
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    results.values = list;
                    results.count = list.size();
                } else {
                    List<ShowInterface> filteredList = new ArrayList<ShowInterface>();
                    for (ShowInterface show : list) {
                        if (show.getShowName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            filteredList.add(show);
                        }
                    }

                    results.values = filteredList;
                    results.count = filteredList.size();

                }
                return results;
            }
        };
    }

}
