package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.UserInterface;

/**
 * Created by Alex on 02/04/2016.
 */
public class User implements UserInterface {

    private static User ourInstance;
    private String userID;
    private String userName;
    private String userPassword;
    private String userAvatar;
    private String userPosition;
    private String userAuthCode;

    private User() {
    }

    public static User getInstance() {
        if (ourInstance == null) {
            ourInstance = new User();
        }
        return ourInstance;
    }

    @Override
    public String getUserID() {
        return this.userID;
    }

    @Override
    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String getUserPassword() {
        return this.userPassword;
    }

    @Override
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public String getUserName() {
        return this.userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getUserAvatar() {
        return this.userAvatar;
    }

    @Override
    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    @Override
    public String getUserPosition() {
        return this.userPosition;
    }

    @Override
    public void setUserPosition(String userPosition) {
        this.userPosition = userPosition;
    }

    @Override
    public String getUserAuthCode() {
        return this.userAuthCode;
    }

    @Override
    public void setUserAuthCode(String authCode) {
        this.userAuthCode = authCode;
    }
}
