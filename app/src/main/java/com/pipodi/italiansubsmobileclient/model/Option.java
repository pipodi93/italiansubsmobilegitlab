package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.OptionInterface;

/**
 * Created by Alex on 13/04/2016.
 */
public class Option implements OptionInterface {

    private String title;
    private String description;
    private OptionTypes type;
    private boolean enabled = true;

    public Option(String title, String description, OptionTypes type) {
        this.title = title;
        this.description = description;
        this.type = type;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public OptionTypes getType() {
        return this.type;
    }

    @Override
    public void setType(OptionTypes type) {
        this.type = type;
    }

    @Override
    public void enable() {
        if (this.enabled) {
            this.enabled = false;
        } else {
            this.enabled = true;
        }
    }
}
