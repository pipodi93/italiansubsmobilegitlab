package com.pipodi.italiansubsmobileclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.model.iterfaces.NewsInterface;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

import java.util.List;

/**
 * Created by pipod on 22/03/2016.
 */
public class NewsAdapter extends BaseAdapter {

    private List<NewsInterface> items;
    private Context context;

    public NewsAdapter(List<NewsInterface> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NewsInterface item = this.items.get(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.news_item_layout, parent, false);
        TextView newsTitle = (TextView) convertView.findViewById(R.id.title_viewer);
        newsTitle.setText(item.getTitle());
        TextView newsEpisode = (TextView) convertView.findViewById(R.id.episode_viewer);
        newsEpisode.setText(item.getEpisode());
        TextView newsRelease = (TextView) convertView.findViewById(R.id.release_date_viewer);
        newsRelease.setText(item.getReleaseDate());
        ImageView newsPhoto = (ImageView) convertView.findViewById(R.id.photo_viewer);
        newsPhoto.setImageBitmap(item.getPhoto());
        if (TempStorage.favouriteShows != null && TempStorage.favouriteShows.contains(item.getTitle().toLowerCase())) {
            newsPhoto.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.favourite_show));
        } else {
            newsPhoto.setBackgroundDrawable(null);
        }
        return convertView;
    }
}