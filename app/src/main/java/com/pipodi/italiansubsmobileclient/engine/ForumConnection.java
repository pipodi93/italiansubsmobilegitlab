package com.pipodi.italiansubsmobileclient.engine;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.activities.MainActivity;
import com.pipodi.italiansubsmobileclient.activities.PostCreationActivity;
import com.pipodi.italiansubsmobileclient.activities.ProfileViewerActivity;
import com.pipodi.italiansubsmobileclient.adapters.ForumAdapter;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.ForumTypes;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumObjInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.PostInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.TopicInterface;
import com.pipodi.italiansubsmobileclient.utils.ResultMapper;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCException;


/**
 * Created by Alex on 13/04/2016.
 */
public class ForumConnection extends AsyncTask<String, String, String> {

    public static final int REQUEST_CODE = 1;
    private static boolean canBackBePressed = false;
    private static int selectedOption = 0;
    private static boolean backPressed = false;
    private static boolean isUnread = false;
    private ForumTypes type;
    private String currentParam;
    private Button nextButton;
    private Button previousButton;
    private Context context;
    private ListView viewList;
    private TapatalkService tapatalkService;
    private Button upButton;
    private Button replyButton;
    private TextView pageIndicator;
    private Button cpButton;
    private SwipeRefreshLayout refreshLayout;
    private ProgressBar progressBar;
    private Button topicButton;
    private List<ForumObjInterface> items;
    private Map<String, Object> childField;
    private ForumAdapter forumAdapter;

    //TODO: Add boolean field to this class/constructor for unread check.

    public ForumConnection(Context context, ListView view, @Nullable Map<String, Object> child, Button upButton,
                           Button replyButton, Button topicButton, Button cpButton, Button nextButton, Button previousButton, SwipeRefreshLayout refreshLayout, ForumTypes type, ProgressBar progressBar,
                           TextView pageIndicator) {
        this.context = context;
        this.viewList = view;
        childField = child;
        items = new ArrayList<>();
        this.upButton = upButton;
        this.type = type;
        this.refreshLayout = refreshLayout;
        this.replyButton = replyButton;
        this.progressBar = progressBar;
        this.nextButton = nextButton;
        this.previousButton = previousButton;
        this.pageIndicator = pageIndicator;
        this.topicButton = topicButton;
        this.cpButton = cpButton;
        MainActivity.forumConnection = this;
    }

    public static boolean canBackBePressed() {
        return canBackBePressed;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        canBackBePressed = false;
        if (!refreshLayout.isRefreshing()) {
            this.progressBar.setVisibility(View.VISIBLE);
        }
        this.viewList.setOnItemClickListener(null);
        this.previousButton.setClickable(false);
        this.nextButton.setClickable(false);
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            this.currentParam = params[0];
            this.tapatalkService = TapatalkService.getInstance();
            switch (type) {
                case FORUM:
                    if (childField == null) {
                        items = ResultMapper.getResultFromArray(tapatalkService.getForum());
                    } else {
                        items = ResultMapper.parseChild(childField);
                        if (TempStorage.startPageForum == 0) {
                            items.addAll(ResultMapper.getResultFromTopicMap(tapatalkService.getStickyTopic(params[0]), true));
                        }
                        items.addAll(ResultMapper.getResultFromTopicMap(tapatalkService.getTopic(params[0]), false));
                    }
                    break;
                case TOPIC:
                    if (TempStorage.startPageForum == 0) {
                        items = ResultMapper.getResultFromTopicMap(tapatalkService.getStickyTopic(params[0]), true);
                    }
                    items.addAll(ResultMapper.getResultFromTopicMap(tapatalkService.getTopic(params[0]), false));
                    break;
                case POST:
                    if (isUnread) {
                        items = ResultMapper.getResultFromPostUnreadMap(tapatalkService.getFirstUnreadPost(params[0]));
                        isUnread = false;
                    } else {
                        TempStorage.firstUnreadPost = 1;
                        items = ResultMapper.getResultFromPostMap(tapatalkService.getPosts(params[0]));
                    }
                    break;
                case UNREAD_TOPICS:
                    items = ResultMapper.getResultFromUnreadTopicMap(tapatalkService.getUnreadThreads());
                    break;
                case UNREAD_REPLIES:
                    items = ResultMapper.getResultFromPartecipatedMap(tapatalkService.getPartecipatedTopic());
                    TempStorage.totalUnreadReplies = items.size();
                    break;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (XMLRPCException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (type.equals(ForumTypes.UNREAD_REPLIES) && this.items.size() == 1 && TempStorage.endPageForum < 39) {
            TempStorage.startPageForum = TempStorage.endPageForum + 1;
            TempStorage.endPageForum = TempStorage.startPageForum + 19;
            ForumConnection moreConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                    nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_REPLIES, progressBar,
                    pageIndicator);
            moreConnection.execute(currentParam);
        } else if (type.equals(ForumTypes.POST) && TempStorage.firstUnreadPost != 1) {
            TempStorage.remainingPosts = TempStorage.totalPostsOnThisThread - (20 * TempStorage.firstUnreadPage);
            TempStorage.currentTopicPage = TempStorage.firstUnreadPage;
            TempStorage.endPagePost = (20 * TempStorage.firstUnreadPage) - 1;
            TempStorage.startPagePost = TempStorage.endPagePost - 19;
            if (TempStorage.firstUnreadPost == TempStorage.firstUnreadPage * 20) {
                TempStorage.itemToScrollTo = items.size() - 1;
            } else {
                TempStorage.itemToScrollTo = TempStorage.firstUnreadPost % 20;
            }
            TempStorage.firstUnreadPost = 1;
            isUnread = false;
            ForumConnection nextConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                    nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                    pageIndicator);
            nextConnection.execute(currentParam);
        } else {
            this.previousButton.setClickable(true);
            this.nextButton.setClickable(true);
            canBackBePressed = true;
            this.progressBar.setVisibility(View.INVISIBLE);
            if (this.refreshLayout.isRefreshing()) {
                refreshLayout.setRefreshing(false);
            }
            checkTopicsNumber();
            if (this.type.equals(ForumTypes.POST)) {
                replyButton.setText("RISPONDI");
                replyButton.setVisibility(View.VISIBLE);
            }

            if ((this.type.equals(ForumTypes.TOPIC) || this.type.equals(ForumTypes.FORUM))) {
                if (TempStorage.startPageForum == 0) {
                    TempStorage.currentForumPage = 1;
                    TempStorage.remainingTopics = TempStorage.totalTopicsOnThisForum;
                    Log.i("TOTALTOPICS: ", "" + TempStorage.totalTopicsOnThisForum);
                } else {
                    TempStorage.remainingTopics = TempStorage.totalTopicsOnThisForum - TempStorage.currentForumPage * 20;
                }
                TempStorage.totalForumPages = (int) Math.ceil(TempStorage.totalTopicsOnThisForum / 20.0);
                Log.i("TOTALPAGES: ", "" + TempStorage.totalForumPages);
            } else if (this.type.equals(ForumTypes.POST)) {
                if (TempStorage.startPagePost == 0) {
                    TempStorage.currentTopicPage = 1;
                    TempStorage.remainingPosts = TempStorage.totalPostsOnThisThread;
                    Log.i("TOTALPOSTS: ", "" + TempStorage.totalPostsOnThisThread);
                } else {
                    TempStorage.remainingPosts = TempStorage.totalPostsOnThisThread - TempStorage.currentTopicPage * 20;
                }
                TempStorage.totalTopicPages = (int) Math.ceil(TempStorage.totalPostsOnThisThread / 20.0);
                Log.i("TOTALPAGES: ", "" + TempStorage.totalTopicPages);
            } else if (this.type.equals(ForumTypes.UNREAD_TOPICS)) {
                if (TempStorage.startPageForum == 0) {
                    TempStorage.currentForumPage = 1;
                    TempStorage.remainingTopics = TempStorage.totalUnreadTopics;
                    Log.i("TOTALTOPICS: ", "" + TempStorage.totalUnreadTopics);
                } else {
                    TempStorage.remainingTopics = TempStorage.totalUnreadTopics - TempStorage.currentForumPage * 20;
                }
                TempStorage.totalForumPages = (int) Math.ceil(TempStorage.totalUnreadTopics / 20.0);
            }
            if (TempStorage.currentTopicPage != TempStorage.totalTopicPages && this.type.equals(ForumTypes.POST) && TempStorage.totalTopicPages != 0 || TempStorage.totalForumPages != 0 && TempStorage.currentForumPage != TempStorage.totalForumPages && (this.type.equals(ForumTypes.TOPIC) || this.type.equals(ForumTypes.FORUM))
                    || TempStorage.currentForumPage != TempStorage.totalForumPages && this.type.equals(ForumTypes.UNREAD_TOPICS) && TempStorage.totalForumPages != 0) {
                nextButton.setVisibility(View.VISIBLE);
            } else {
                nextButton.setVisibility(View.INVISIBLE);
            }

            if (TempStorage.startPageForum == 0 && this.type.equals(ForumTypes.FORUM) || TempStorage.startPagePost == 0 && this.type.equals(ForumTypes.POST)
                    || TempStorage.startPageForum == 0 && (this.type.equals(ForumTypes.TOPIC) || this.type.equals(ForumTypes.STICKY_TOPIC) || this.type.equals(ForumTypes.UNREAD_TOPICS))) {
                previousButton.setVisibility(View.INVISIBLE);
            } else {
                previousButton.setVisibility(View.VISIBLE);
            }

            if (backPressed) {
                removeButtons();
                backPressed = false;
            }

            setPages();

            if (this.type.equals(ForumTypes.POST) && items.size() > 0) {
                PostInterface item = (PostInterface) items.get(0);
                /*if ((TempStorage.translationList.contains(item.getForumID()) && TempStorage.allowedUserPositionsList.contains(User.getInstance().getUserPosition()))) {
                    cpButton.setText("PARTECIPA");
                }*/
            } else {
                cpButton.setText("PROFILO");
            }

            forumAdapter = new ForumAdapter(this.context, items);
            viewList.setAdapter(forumAdapter);
            if (TempStorage.itemToScrollTo != 0) {
                viewList.setSelection(TempStorage.itemToScrollTo);
                TempStorage.itemToScrollTo = 0;
            }
            viewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    upButton.setText("TORNA SU");
                    ForumObjInterface currentItem = items.get(position);
                    switch (currentItem.getType()) {
                        case FORUM:
                            TempStorage.firstUnreadPost = 0;
                            ForumInterface currentForum = (ForumInterface) currentItem;
                            Map<String, Object> child = currentForum.getChild();
                            if (child.size() > 0) {
                                TempStorage.previousForum = currentForum.getForumID();
                                TempStorage.forumHistory.push(items);
                                TempStorage.paramsHistory.push(currentParam);
                                TempStorage.previousFourmPage = TempStorage.currentForumPage;
                                TempStorage.totalPageHistory.push((int) Math.ceil(TempStorage.totalTopicsOnThisForum / 20.0));
                                ForumConnection forumConnection = new ForumConnection(context, viewList, child, upButton, replyButton, topicButton, cpButton,
                                        nextButton, previousButton, refreshLayout, ForumTypes.FORUM, progressBar,
                                        pageIndicator);
                                forumConnection.execute(currentForum.getForumID());
                            } else {
                                TempStorage.previousForum = currentForum.getForumID();
                                TempStorage.forumHistory.push(items);
                                TempStorage.paramsHistory.push(currentParam);
                                TempStorage.previousFourmPage = TempStorage.currentForumPage;
                                TempStorage.totalPageHistory.push((int) Math.ceil(TempStorage.totalTopicsOnThisForum / 20.0));
                                ForumConnection threadConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                                        nextButton, previousButton, refreshLayout, ForumTypes.TOPIC, progressBar,
                                        pageIndicator);
                                threadConnection.execute(currentForum.getForumID());
                            }
                            break;
                        case TOPIC:
                            TempStorage.firstUnreadPost = 0;
                            TempStorage.currentTopicPage = 1;
                            TempStorage.startPagePost = 0;
                            TempStorage.endPagePost = 19;
                            TopicInterface currentTopic = (TopicInterface) currentItem;
                            TempStorage.forumHistory.push(items);
                            TempStorage.paramsHistory.push(currentParam);
                            TempStorage.tempChild = childField;
                            TempStorage.totalPageHistory.push((int) Math.ceil(TempStorage.totalTopicsOnThisForum / 20.0));
                            ForumConnection postConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                                    nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                                    pageIndicator);
                            postConnection.execute(currentTopic.getTopicID());
                            break;
                        case STICKY_TOPIC:
                            TempStorage.firstUnreadPost = 0;
                            TempStorage.currentTopicPage = 1;
                            TempStorage.startPagePost = 0;
                            TempStorage.endPagePost = 19;
                            TopicInterface currentStickyTopic = (TopicInterface) currentItem;
                            TempStorage.forumHistory.push(items);
                            TempStorage.paramsHistory.push(currentParam);
                            TempStorage.tempChild = childField;
                            TempStorage.totalPageHistory.push((int) Math.ceil(TempStorage.totalTopicsOnThisForum / 20.0));
                            ForumConnection stickyPostConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                                    nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                                    pageIndicator);
                            stickyPostConnection.execute(currentStickyTopic.getTopicID());
                            break;
                        case POST:
                            TempStorage.firstUnreadPost = 0;
                            break;
                        case UNREAD_REPLIES:
                            TopicInterface currentUnreadReplies = (TopicInterface) currentItem;
                            TempStorage.forumHistory.push(items);
                            TempStorage.paramsHistory.push(currentParam);
                            TempStorage.tempChild = childField;
                            isUnread = true;
                            TempStorage.totalPageHistory.push((int) Math.ceil(TempStorage.totalUnreadReplies / 20.0));
                            ForumConnection repliesConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                    nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                                    pageIndicator);
                            repliesConnection.execute(currentUnreadReplies.getTopicID());
                            break;
                        case UNREAD_TOPICS:
                            TopicInterface currentUnreadTopics = (TopicInterface) currentItem;
                            TempStorage.forumHistory.push(items);
                            TempStorage.paramsHistory.push(currentParam);
                            TempStorage.tempChild = childField;
                            isUnread = true;
                            TempStorage.totalPageHistory.push((int) Math.ceil(TempStorage.totalUnreadTopics / 20.0));
                            ForumConnection topicsConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                    nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                                    pageIndicator);
                            topicsConnection.execute(currentUnreadTopics.getTopicID());
                            break;
                        case OPTION:
                            TempStorage.startPageForum = TempStorage.endPageForum + 1;
                            TempStorage.endPageForum = TempStorage.startPageForum + 19;
                            ForumConnection moreConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                    nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_REPLIES, progressBar,
                                    pageIndicator);
                            moreConnection.execute(currentParam);
                            break;
                    }
                }
            });

            viewList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    ForumObjInterface currentItem = items.get(position);
                    if (currentItem.getType().equals(ForumTypes.POST)) {
                        view.setSelected(true);
                        final PostInterface currentPost = (PostInterface) currentItem;
                        new AsyncTask<String, String, String>() {
                            @Override
                            protected String doInBackground(String... params) {
                                try {
                                    return ResultMapper.parsePostQuote(tapatalkService.getQuotePost(currentPost.getPostID()));
                                } catch (XMLRPCException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                Intent intent = new Intent(context, PostCreationActivity.class);
                                intent.putExtra("QUOTE", s);
                                intent.putExtra("TYPE", "POST");
                                intent.putExtra("ID", currentParam);
                                intent.putExtra("TITLE", currentPost.getPostTitle());
                                intent.putExtra("TOPICID", currentPost.getTopicID());
                                intent.putExtra("FORUMID", currentPost.getForumID());
                                TempStorage.tempList = items;
                                TempStorage.tempParam = currentParam;
                                TempStorage.tempChild = childField;
                                MainActivity.activity.startActivityForResult(intent, REQUEST_CODE);
                            }
                        }.execute("");
                    }
                    return false;
                }
            });
            upButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (upButton.getText().toString().equals("TORNA SU")) {
                        backOnForum();
                        TempStorage.currentTopicPage = 1;
                        TempStorage.startPagePost = 0;
                        TempStorage.endPagePost = 19;
                    } else {
                        ForumConnection forumConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                                nextButton, previousButton, refreshLayout, ForumTypes.FORUM, progressBar,
                                pageIndicator);
                        forumConnection.execute("");
                        TempStorage.startPageForum = 0;
                        TempStorage.endPageForum = 19;
                        TempStorage.totalTopicsOnThisForum = 0;
                        upButton.setText("TORNA SU");
                    }
                }
            });

            replyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PostCreationActivity.class);
                    if (replyButton.getText().equals("NUOVO TOPIC")) {
                        intent.putExtra("TYPE", "TOPIC");
                        intent.putExtra("ID", currentParam);
                        TempStorage.tempList = items;
                        TempStorage.tempParam = currentParam;
                        TempStorage.tempChild = childField;
                        MainActivity.activity.startActivityForResult(intent, REQUEST_CODE);
                    } else {
                        PostInterface firstPost = (PostInterface) items.get(0);
                        intent.putExtra("TYPE", "POST");
                        intent.putExtra("TOPICID", firstPost.getTopicID());
                        intent.putExtra("ID", firstPost.getForumID());
                        intent.putExtra("TITLE", firstPost.getPostTitle());
                        TempStorage.tempList = items;
                        TempStorage.tempParam = currentParam;
                        TempStorage.tempChild = childField;
                        MainActivity.activity.startActivityForResult(intent, REQUEST_CODE);
                    }
                }
            });

            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (type.equals(ForumTypes.FORUM) || type.equals(ForumTypes.TOPIC)) {
                        TempStorage.remainingTopics -= 20;
                        TempStorage.currentForumPage++;
                        TempStorage.startPageForum = TempStorage.endPageForum + 1;
                        TempStorage.endPageForum = TempStorage.startPageForum + 19;
                        ForumConnection nextConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                nextButton, previousButton, refreshLayout, ForumTypes.TOPIC, progressBar,
                                pageIndicator);
                        nextConnection.execute(currentParam);
                    } else if (type.equals(ForumTypes.POST)) {
                        TempStorage.remainingPosts -= 20;
                        TempStorage.currentTopicPage++;
                        TempStorage.startPagePost = TempStorage.endPagePost + 1;
                        TempStorage.endPagePost = TempStorage.startPagePost + 19;
                        ForumConnection nextConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                                pageIndicator);
                        nextConnection.execute(currentParam);
                    } else if (type.equals(ForumTypes.UNREAD_TOPICS)) {
                        TempStorage.remainingTopics -= 20;
                        TempStorage.currentForumPage++;
                        TempStorage.startPageForum = TempStorage.endPageForum + 1;
                        TempStorage.endPageForum = TempStorage.startPageForum + 19;
                        ForumConnection nextConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_TOPICS, progressBar,
                                pageIndicator);
                        nextConnection.execute(currentParam);

                    } else if (type.equals(ForumTypes.UNREAD_REPLIES)) {
                        TempStorage.remainingTopics -= 20;
                        TempStorage.currentForumPage++;
                        TempStorage.startPageForum = TempStorage.endPageForum + 1;
                        TempStorage.endPageForum = TempStorage.startPageForum + 19;
                        ForumConnection nextConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_REPLIES, progressBar,
                                pageIndicator);
                        nextConnection.execute(currentParam);
                    }
                }
            });

            previousButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (type.equals(ForumTypes.FORUM) || type.equals(ForumTypes.TOPIC)) {
                        TempStorage.remainingTopics += 20;
                        TempStorage.currentForumPage--;
                        TempStorage.startPageForum = TempStorage.startPageForum - 20;
                        TempStorage.endPageForum = TempStorage.startPageForum + 19;
                        if (type.equals(ForumTypes.FORUM)) {
                            ForumConnection previousConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                    nextButton, previousButton, refreshLayout, ForumTypes.FORUM, progressBar,
                                    pageIndicator);
                            previousConnection.execute(currentParam);
                        } else if (type.equals(ForumTypes.TOPIC)) {
                            ForumConnection previousConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                    nextButton, previousButton, refreshLayout, ForumTypes.TOPIC, progressBar,
                                    pageIndicator);
                            previousConnection.execute(currentParam);
                        }
                    } else if (type.equals(ForumTypes.POST)) {
                        TempStorage.remainingTopics += 20;
                        TempStorage.currentTopicPage--;
                        TempStorage.startPagePost = TempStorage.startPagePost - 20;
                        TempStorage.endPagePost = TempStorage.endPagePost + 19;
                        ForumConnection nextConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                                pageIndicator);
                        nextConnection.execute(currentParam);
                    } else if (type.equals(ForumTypes.UNREAD_TOPICS)) {
                        TempStorage.remainingTopics += 20;
                        TempStorage.currentForumPage--;
                        TempStorage.startPageForum = TempStorage.startPageForum - 20;
                        TempStorage.endPageForum = TempStorage.startPageForum + 19;
                        ForumConnection nextConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_TOPICS, progressBar,
                                pageIndicator);
                        nextConnection.execute(currentParam);

                    } else if (type.equals(ForumTypes.UNREAD_REPLIES)) {
                        TempStorage.remainingTopics += 20;
                        TempStorage.currentForumPage--;
                        TempStorage.startPageForum = TempStorage.startPageForum - 20;
                        TempStorage.endPageForum = TempStorage.startPageForum + 19;
                        ForumConnection nextConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_REPLIES, progressBar,
                                pageIndicator);
                        nextConnection.execute(currentParam);
                    }
                }
            });

            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    setTypeForRefresh(false);
                }
            });

            topicButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(MainActivity.context)
                            .setTitle("Cosa vuoi leggere?")
                            .setItems(R.array.topicChoices, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    selectedOption = i;
                                    if (selectedOption == 0) {
                                        dialogInterface.dismiss();
                                        TempStorage.startPageForum = 0;
                                        TempStorage.endPageForum = 19;
                                        upButton.setText("HOME");
                                        cpButton.setText("PROFILO");
                                        TempStorage.forumHistory.push(items);
                                        TempStorage.paramsHistory.push(currentParam);
                                        TempStorage.tempChild = childField;
                                        ForumConnection forumConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                                nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_TOPICS, progressBar,
                                                pageIndicator);
                                        forumConnection.execute("");
                                    } else {
                                        dialogInterface.dismiss();
                                        TempStorage.startPageForum = 0;
                                        TempStorage.endPageForum = 19;
                                        ResultMapper.clearTopicList();
                                        upButton.setText("HOME");
                                        cpButton.setText("PROFILO");
                                        TempStorage.forumHistory.push(items);
                                        TempStorage.paramsHistory.push(currentParam);
                                        TempStorage.tempChild = childField;
                                        ForumConnection forumConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                                                nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_REPLIES, progressBar,
                                                pageIndicator);
                                        forumConnection.execute("");
                                    }
                                }
                            }).setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
                }
            });

            cpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cpButton.getText().equals("PROFILO")) {
                        Intent intent = new Intent(context, ProfileViewerActivity.class);
                        intent.putExtra("USERID", User.getInstance().getUserID());
                        MainActivity.activity.startActivity(intent);
                    } else {
                        PostInterface firstPost = (PostInterface) items.get(0);
                        new AsyncTask<String, Void, Void>() {

                            private DefaultHttpClient httpClient = new DefaultHttpClient();
                            private HttpGet httpGet = null;

                            @Override
                            protected Void doInBackground(String... params) {
                                httpGet = new HttpGet(
                                        "http://www.italiansubs.net/forum/?action=traduzioni;sa=add;topic=" + params[0]);
                                httpGet.addHeader("Cookie", LoginVariables.cookieString);
                                Log.i("ADDTRADDA", "http://www.italiansubs.net/forum/?action=traduzioni;sa=add;topic=" + params[0]);
                                try {
                                    HttpResponse response = httpClient.execute(httpGet);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                Toast.makeText(context, "Traduzione aggiunta correttamente!", Toast.LENGTH_LONG).show();
                            }
                        }.execute(firstPost.getTopicID());
                    }
                }
            });
        }
        pageIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View layout = LayoutInflater.from(MainActivity.context).inflate(R.layout.page_selector_dialog, null);
                new AlertDialog.Builder(MainActivity.activity).setView(layout)
                        .setTitle("Salta a pagina...")
                        .setMessage("Inserisci una pagina")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                TextView pageField = (TextView) layout.findViewById(R.id.pageNumberField);
                                int pageNumber = Integer.parseInt(pageField.getText().toString());
                                if (type.equals(ForumTypes.TOPIC) || type.equals(ForumTypes.FORUM)) {
                                    if (pageNumber > TempStorage.totalForumPages || pageNumber <= 0) {
                                        Toast.makeText(MainActivity.context, "Numero di pagina non valido.", Toast.LENGTH_LONG).show();
                                    } else {
                                        TempStorage.endPageForum = (pageNumber * 20) - 1;
                                        TempStorage.startPageForum = TempStorage.endPageForum - 19;
                                        TempStorage.currentForumPage = pageNumber;
                                        ForumConnection threadConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                                                nextButton, previousButton, refreshLayout, ForumTypes.TOPIC, progressBar,
                                                pageIndicator);
                                        threadConnection.execute(currentParam);
                                    }
                                } else if (type.equals(ForumTypes.POST)) {
                                    if (pageNumber > TempStorage.totalTopicPages || pageNumber <= 0) {
                                        Toast.makeText(MainActivity.context, "Numero di pagina non valido.", Toast.LENGTH_LONG).show();
                                    } else {
                                        TempStorage.endPagePost = (pageNumber * 20) - 1;
                                        TempStorage.startPagePost = TempStorage.endPagePost - 19;
                                        TempStorage.currentTopicPage = pageNumber;
                                        PostInterface post = (PostInterface) items.get(0);
                                        ForumConnection postConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                                                nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                                                pageIndicator);
                                        postConnection.execute(post.getTopicID());
                                        dialogInterface.dismiss();
                                    }
                                } else if (type.equals(ForumTypes.UNREAD_REPLIES)) {
                                    if (pageNumber > TempStorage.totalForumPages || pageNumber <= 0) {
                                        Toast.makeText(MainActivity.context, "Numero di pagina non valido.", Toast.LENGTH_LONG).show();
                                    } else {
                                        TempStorage.endPageForum = (pageNumber * 20) - 1;
                                        TempStorage.startPageForum = TempStorage.endPageForum - 19;
                                        TempStorage.currentForumPage = pageNumber;
                                        ForumConnection threadConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                                                nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_REPLIES, progressBar,
                                                pageIndicator);
                                        threadConnection.execute(currentParam);
                                    }
                                } else if (type.equals(ForumTypes.UNREAD_TOPICS)) {
                                    if (pageNumber > TempStorage.totalForumPages || pageNumber <= 0) {
                                        Toast.makeText(MainActivity.context, "Numero di pagina non valido.", Toast.LENGTH_LONG).show();
                                    } else {
                                        TempStorage.endPageForum = (pageNumber * 20) - 1;
                                        TempStorage.startPageForum = TempStorage.endPageForum - 19;
                                        TempStorage.currentForumPage = pageNumber;
                                        ForumConnection threadConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                                                nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_TOPICS, progressBar,
                                                pageIndicator);
                                        threadConnection.execute(currentParam);
                                    }
                                }
                            }
                        })
                        .setNegativeButton("ANNULLA", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).create().show();
            }
        });
    }

    private void refreshCurrentView() {
        refreshLayout.setRefreshing(true);
        switch (type) {
            case FORUM:
                if (childField != null) {
                    ForumConnection forumConnection = new ForumConnection(context, viewList, childField, upButton, replyButton, topicButton, cpButton,
                            nextButton, previousButton, refreshLayout, ForumTypes.FORUM, progressBar,
                            pageIndicator);
                    forumConnection.execute(currentParam);
                } else {
                    ForumConnection threadConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                            nextButton, previousButton, refreshLayout, ForumTypes.FORUM, progressBar,
                            pageIndicator);
                    threadConnection.execute("-1");
                }
                break;
            case TOPIC:
                ForumConnection threadConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                        nextButton, previousButton, refreshLayout, ForumTypes.TOPIC, progressBar,
                        pageIndicator);
                threadConnection.execute(currentParam);
                break;
            case POST:
                ForumConnection postConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                        nextButton, previousButton, refreshLayout, ForumTypes.POST, progressBar,
                        pageIndicator);
                postConnection.execute(currentParam);
                break;
            case UNREAD_REPLIES:
                ResultMapper.clearTopicList();
                ForumConnection unreadRepliesConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                        nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_REPLIES, progressBar,
                        pageIndicator);
                unreadRepliesConnection.execute(currentParam);
                break;
            case UNREAD_TOPICS:
                ForumConnection unreadTopicConnection = new ForumConnection(context, viewList, null, upButton, replyButton, topicButton, cpButton,
                        nextButton, previousButton, refreshLayout, ForumTypes.UNREAD_TOPICS, progressBar,
                        pageIndicator);
                unreadTopicConnection.execute(currentParam);
                break;
        }
    }

    public void setTypeForRefresh(Boolean fromPost) {
        int forumCounter = 0;
        int topicCounter = 0;
        int postCounter = 0;
        int unreadTopicCounter = 0;
        int unreadRepliesCounter = 0;
        if (fromPost) {
            items = TempStorage.tempList;
            this.currentParam = TempStorage.tempParam;
            childField = TempStorage.tempChild;
        }
        for (ForumObjInterface forumObjInterface : items) {
            switch (forumObjInterface.getType()) {
                case FORUM:
                    forumCounter++;
                    break;
                case TOPIC:
                    topicCounter++;
                    break;
                case STICKY_TOPIC:
                    topicCounter++;
                    break;
                case POST:
                    postCounter++;
                    break;
                case UNREAD_REPLIES:
                    unreadRepliesCounter++;
                    break;
                case UNREAD_TOPICS:
                    unreadTopicCounter++;
                    break;
            }
        }
        if (forumCounter > 0) {
            type = ForumTypes.FORUM;
        } else if (topicCounter > 0 && forumCounter == 0) {
            type = ForumTypes.TOPIC;
        } else if (forumCounter == 0 && topicCounter == 0 && postCounter > 0) {
            type = ForumTypes.POST;
        } else if (forumCounter == 0 && topicCounter == 0 && postCounter == 0 && unreadRepliesCounter > 0) {
            type = ForumTypes.UNREAD_REPLIES;
        } else if (forumCounter == 0 && topicCounter == 0 && postCounter == 0 && unreadRepliesCounter == 0 && unreadTopicCounter > 0) {
            type = ForumTypes.UNREAD_TOPICS;
        }
        refreshCurrentView();
    }

    public void removeButtons() {
        nextButton.setVisibility(View.INVISIBLE);
        previousButton.setVisibility(View.INVISIBLE);
    }

    public void setBackPressed() {
        backPressed = true;
    }

    private void checkTopicsNumber() {
        int topicCounter = 0;
        if (items != null) {
            for (ForumObjInterface forumObjInterface : items) {
                if (forumObjInterface.getType().equals(ForumTypes.TOPIC)
                        || forumObjInterface.getType().equals(ForumTypes.STICKY_TOPIC)
                        || forumObjInterface.getType().equals(ForumTypes.UNREAD_TOPICS)
                        || forumObjInterface.getType().equals(ForumTypes.UNREAD_REPLIES)) {
                    topicCounter++;
                }
            }
            if (topicCounter < 1) {
                nextButton.setVisibility(View.INVISIBLE);
            } else {
                nextButton.setVisibility(View.VISIBLE);
            }

            if (TempStorage.startPageForum != 0) {
                previousButton.setVisibility(View.VISIBLE);
            }

            if (!TempStorage.canUserPost || topicCounter == 0) {
                replyButton.setVisibility(View.GONE);
            } else {
                replyButton.setVisibility(View.VISIBLE);
                replyButton.setText("NUOVO TOPIC");
                cpButton.setText("PROFILO");
            }
        }
    }

    public void backOnForum() {
        if (!TempStorage.forumHistory.empty()) {
            List<ForumObjInterface> newItems = TempStorage.forumHistory.pop();
            if (type.equals(ForumTypes.POST)) {
                if (newItems.get(0).getType().equals(ForumTypes.UNREAD_TOPICS)) {
                    type = ForumTypes.UNREAD_TOPICS;
                } else if (newItems.get(0).getType().equals(ForumTypes.UNREAD_REPLIES)) {
                    type = ForumTypes.UNREAD_REPLIES;
                } else {
                    type = ForumTypes.TOPIC;
                }
                TempStorage.currentTopicPage = 1;
                if (!TempStorage.totalPageHistory.isEmpty()) {
                    TempStorage.totalForumPages = TempStorage.totalPageHistory.pop();
                }
                if (TempStorage.totalForumPages == 0) {
                    previousButton.setVisibility(View.INVISIBLE);
                    nextButton.setVisibility(View.INVISIBLE);
                    TempStorage.startPageForum = 0;
                    TempStorage.endPageForum = 19;
                    TempStorage.currentForumPage = 1;
                    cpButton.setText("PROFILO");
                }
            } else if (type.equals(ForumTypes.TOPIC) || type.equals(ForumTypes.UNREAD_REPLIES) || type.equals(ForumTypes.UNREAD_TOPICS) || type.equals(ForumTypes.FORUM)) {
                type = ForumTypes.FORUM;
                if (!TempStorage.totalPageHistory.isEmpty()) {
                    TempStorage.totalForumPages = TempStorage.totalPageHistory.pop();
                } else {
                    TempStorage.totalForumPages = 0;
                }
                if (TempStorage.totalForumPages == 0) {
                    previousButton.setVisibility(View.INVISIBLE);
                    nextButton.setVisibility(View.INVISIBLE);
                    TempStorage.startPageForum = 0;
                    TempStorage.endPageForum = 19;
                    TempStorage.currentForumPage = 1;
                    TempStorage.totalTopicsOnThisForum = 0;
                }
            }
            currentParam = TempStorage.paramsHistory.pop();
            childField = TempStorage.tempChild;
            items.clear();
            items.addAll(newItems);
            forumAdapter.notifyDataSetChanged();
            checkTopicsNumber();
            setPages();
        }
    }

    private void setPages() {
        if (nextButton.getVisibility() == View.INVISIBLE && previousButton.getVisibility() == View.INVISIBLE && this.type != ForumTypes.UNREAD_REPLIES) {
            pageIndicator.setText("");
        } else if (type.equals(ForumTypes.FORUM) || type.equals(ForumTypes.TOPIC) || type.equals(ForumTypes.STICKY_TOPIC)) {
            if (TempStorage.totalForumPages > 0) {
                pageIndicator.setText(TempStorage.currentForumPage + "/" + TempStorage.totalForumPages);
                if (TempStorage.currentForumPage == 1) {
                    previousButton.setVisibility(View.INVISIBLE);
                }
            } else {
                pageIndicator.setText("");
            }
        } else if (type.equals(ForumTypes.POST)) {
            if (TempStorage.totalTopicPages > 0) {
                pageIndicator.setText(TempStorage.currentTopicPage + "/" + TempStorage.totalTopicPages);
                if (TempStorage.currentTopicPage == 1) {
                    previousButton.setVisibility(View.INVISIBLE);
                }
            } else {
                pageIndicator.setText("1/1");
            }
        } else if (type.equals(ForumTypes.UNREAD_TOPICS)) {
            if (TempStorage.totalUnreadTopics > 0) {
                pageIndicator.setText(TempStorage.currentForumPage + "/" + TempStorage.totalForumPages);
                if (TempStorage.currentForumPage == 1) {
                    previousButton.setVisibility(View.INVISIBLE);
                }
            } else {
                pageIndicator.setText("");
            }
        } else if (type.equals(ForumTypes.UNREAD_REPLIES)) {
            pageIndicator.setText("");
            previousButton.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.INVISIBLE);
        }
    }

    private void SendLogcatMail() {

        // save logcat in file
        File outputFile = new File(Environment.getExternalStorageDirectory(),
                "logcat.txt");
        try {
            Runtime.getRuntime().exec(
                    "logcat -f " + outputFile.getAbsolutePath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //send file using email
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // Set type to "email"
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {"pipodi93@gmail.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        // the attachment
        Uri uri = Uri.fromFile(outputFile);
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Italiansubs Mobile Logcat");
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }
}