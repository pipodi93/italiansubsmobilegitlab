package com.pipodi.italiansubsmobileclient.connections;

/**
 * Created by Alex on 22/03/2016.
 */

import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class HTTPS {

    public static HTTPS connection = null;
    private SSLContext ssl;

    private HTTPS() throws NoSuchAlgorithmException, KeyManagementException {
        this.ssl = SSLContext.getInstance("TLS");
        this.ssl.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager() {
                }},
                new SecureRandom());
        SSLContext.setDefault(this.ssl);
    }

    public static HTTPS getInstance() throws NoSuchAlgorithmException, KeyManagementException {
        if (connection == null) {
            connection = new HTTPS();
        }
        return connection;
    }

    public InputStream connection(String url) throws Exception {
        URL u = new URL(url);
        HttpsURLConnection conn = (HttpsURLConnection) u.openConnection();
        conn.setRequestProperty("User-Agent", "ItaSA Mobile Test Application by Pipodi");
        conn.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        });
        if (conn.getResponseCode() == 200) {
            return conn.getInputStream();
        }
        return null;
    }

    private class DefaultTrustManager implements X509TrustManager {
        private DefaultTrustManager() {
        }

        public void checkClientTrusted(X509Certificate[] certs, String authtype)
                throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authtype)
                throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
}