package com.pipodi.italiansubsmobileclient.model.iterfaces;

import com.pipodi.italiansubsmobileclient.model.ForumTypes;

/**
 * Created by Alex on 13/04/2016.
 */
public interface PostInterface {

    String getPostID();

    String getPostText();

    String getPostAuthor();

    String getPostDate();

    ForumTypes getType();

    String getForumID();

    String getTopicID();

    String getPostTitle();

    String getPostAuthorID();

    String getAvatarURL();

}
