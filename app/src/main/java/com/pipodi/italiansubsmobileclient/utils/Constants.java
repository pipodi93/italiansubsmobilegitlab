package com.pipodi.italiansubsmobileclient.utils;

/**
 * Created by pipod on 27/03/2016.
 */
public class Constants {
    public final static String APIKey = "ce0a811540244ad5267785731a5c37a0";
    public static final String SHARED_PREFS_NAME = "MyItaSAPrefs";
    public static boolean showsListDownloaded = false;
    public static boolean newsDownloaded = false;
    public static boolean myItaSADownloaded = false;
}
