package com.pipodi.italiansubsmobileclient.engine;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.adapters.ActorsAdapter;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.ImageConnection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.Actor;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ActorInterface;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 02/04/2016.
 */
public class ActorsParser extends AsyncTask<String, String, String> {

    private ProgressBar progressBar;
    private Context context;
    private List<ActorInterface> list;
    private ListView listView;
    private ActorsAdapter actorsAdapter;
    private int showID;
    private boolean error;

    public ActorsParser(int showID, ListView listView, Context context, ProgressBar progressBar) {
        super();
        this.showID = showID;
        this.context = context;
        this.listView = listView;
        this.progressBar = progressBar;
    }

    @Override
    protected String doInBackground(String... params) {
        this.list = new ArrayList<>();
        Document document = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/shows/" + this.showID
                + "?apikey=" + Constants.APIKey, context);
        if (document == null || !LoginVariables.isRequestValid(document)) {
            this.error = true;
        } else {
            Element item = (Element) XMLIterator.parseXMLTree(1, document).next();
            if (item.getChild("actors") != null) {
                Iterator temp = item.getChild("actors").getChildren().iterator();
                while (temp.hasNext()) {
                    Element tempItem = (Element) temp.next();
                    String name = tempItem.getChild("name").getText();
                    String role = tempItem.getChild("as").getText();
                    Bitmap photo = ImageConnection.getPhotoFromURL(tempItem.getChild("image").getText());
                    ActorInterface actor = new Actor(name, role, photo);
                    this.list.add(actor);
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.progressBar.setVisibility(View.INVISIBLE);
        if (!this.error) {
            this.actorsAdapter = new ActorsAdapter(context, list);
            this.listView.setAdapter(actorsAdapter);
        } else {
            Toast.makeText(context, "Errore durante il download. Si prega di riprovare più tardi.", Toast.LENGTH_LONG).show();
        }
    }
}
