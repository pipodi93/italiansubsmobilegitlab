package com.pipodi.italiansubsmobileclient.model;

import android.graphics.Bitmap;

import com.pipodi.italiansubsmobileclient.model.iterfaces.ActorInterface;

/**
 * Created by Alex on 02/04/2016.
 */
public class Actor implements ActorInterface {
    private String name;
    private String role;
    private Bitmap photo;

    public Actor(String name, String role, Bitmap photo) {
        this.name = name;
        this.role = role;
        this.photo = photo;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public Bitmap getPhoto() {
        return this.photo;
    }
}
