package com.pipodi.italiansubsmobileclient.model.iterfaces;

/**
 * Created by pipod on 27/03/2016.
 */
public interface ShowInterface {

    String getShowName();

    void setShowName(String showName);

    int getShowID();

    void getShowID(int ID);
}
