package com.pipodi.italiansubsmobileclient.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.SubtitlesParser;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

/**
 * Created by Alex on 11/04/2016.
 */
public class SubtitleFragment extends Fragment {

    private Context context;
    private EditText titleBox;
    private EditText episodeBox;
    private EditText seasonBox;
    private CheckBox completeBox;
    private TextView warningText;
    private Button versionButton;
    private int selectedVersion;
    private String version = "Qualsiasi";
    private ListView subListView;
    private FloatingActionButton searchButton;
    private Button nextPageButton;
    private Button previousPageButton;
    private String show_name = "";
    private String season = "";
    private boolean choice = false;
    private boolean complete = false;
    private String seasonAndEpisode = "";


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View generalView = inflater.inflate(R.layout.subtitle_fragment_layout, container, false);
        titleBox = (EditText) generalView
                .findViewById(R.id.titleBox);
        episodeBox = (EditText) generalView
                .findViewById(R.id.episodeBox);
        seasonBox = (EditText) generalView
                .findViewById(R.id.seasonBox);
        completeBox = (CheckBox) generalView.findViewById(R.id.complete_checkbox);
        warningText = (TextView) generalView.findViewById(R.id.warningTextSub);

        TempStorage.currentSubPage = 1;
        versionButton = (Button) generalView.findViewById(R.id.versionButton);
        final String[] versions = getResources().getStringArray(R.array.versions);
        versionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setTitle("Versione:")
                        .setItems(R.array.versions, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                selectedVersion = i;
                                version = versions[i];
                                dialogInterface.dismiss();
                                setVersionIcon(version);
                            }
                        })
                        .create().show();
            }
        });

        TextView versionText = (TextView) generalView.findViewById(R.id.versionText);
        versionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setTitle("Versione:")
                        .setItems(R.array.versions, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                selectedVersion = i;
                                version = versions[i];
                                dialogInterface.dismiss();
                                setVersionIcon(version);
                            }
                        })
                        .create().show();
            }
        });
        subListView = (ListView) generalView
                .findViewById(R.id.subtitles_list);
        final ProgressBar progressBar = (ProgressBar) generalView.findViewById(R.id.progressBar);
        searchButton = (FloatingActionButton) generalView
                .findViewById(R.id.search_button);
        searchButton.animate();
        nextPageButton = (Button) generalView.findViewById(R.id.nextSubPage);
        previousPageButton = (Button) generalView.findViewById(R.id.previousSubPage);
        nextPageButton.setVisibility(View.INVISIBLE);
        previousPageButton.setVisibility(View.INVISIBLE);
        searchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hideKeyboard(context);
                if (titleBox.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Inserisci un titolo.", Toast.LENGTH_LONG).show();
                } else {
                    if (!titleBox.getText().toString().equals("") && !seasonBox.getText().toString().equals("") &&
                            !episodeBox.getText().toString().equals("") && completeBox.isChecked()) {
                        completeBox.setChecked(false);
                    }
                    if (!titleBox.getText().toString().equals("") && completeBox.isChecked()) {
                        show_name = titleBox.getText().toString();
                        season = seasonBox.getText().toString();
                        choice = false;
                        complete = true;
                    } else if (seasonBox.getText().toString().equals("")
                            && episodeBox.getText().toString().equals("")) {
                        show_name = titleBox.getText().toString();
                        choice = false;
                        complete = false;
                    } else if (episodeBox.getText().toString().equals("") && completeBox.isChecked()) {
                        show_name = titleBox.getText().toString();
                        complete = true;
                        season = seasonBox.getText().toString();
                        choice = false;
                    } else if (!seasonBox.getText().equals("") && episodeBox.getText().equals("") && !completeBox.isChecked()) {
                        show_name = titleBox.getText().toString();
                        season = seasonBox.getText().toString();
                        choice = false;
                        complete = false;
                    } else {
                        String episode;
                        if (!episodeBox.getText().toString().equals("") && Math.floor(Math.log10(Integer.parseInt(episodeBox.getText().toString())) + 1) == 1) {
                            episode = "0" + Integer.parseInt(episodeBox.getText().toString());
                        } else {
                            episode = episodeBox.getText().toString();
                        }
                        show_name = titleBox.getText().toString();
                        seasonAndEpisode = seasonBox.getText().toString() + "X" + episode;
                        choice = true;
                    }
                    TempStorage.currentSubPage = 1;
                    nextPageButton.setVisibility(View.GONE);
                    previousPageButton.setVisibility(View.GONE);
                    SubtitlesParser subParser = new SubtitlesParser(
                            subListView, context, inflater, show_name, 0, season, seasonAndEpisode,
                            generalView, choice, complete, version, TempStorage.currentSubPage, nextPageButton, previousPageButton, progressBar);
                    subParser.execute("");
                }
            }
        });
        return generalView;
    }

    private void setVersionIcon(String charSequence) {
        switch (charSequence) {
            case "Qualsiasi":
                versionButton.setBackgroundResource(R.drawable.version_all);
                break;
            case "Normale":
                versionButton.setBackgroundResource(R.drawable.version_sd);
                break;
            case "720p":
                versionButton.setBackgroundResource(R.drawable.version_hd);
                break;
            case "1080i":
                versionButton.setBackgroundResource(R.drawable.version_i);
                break;
            case "1080p":
                versionButton.setBackgroundResource(R.drawable.version_full);
                break;
            case "BluRay":
                versionButton.setBackgroundResource(R.drawable.version_bd);
                break;
            case "BDRip":
                versionButton.setBackgroundResource(R.drawable.version_bd);
                break;
            case "DVDRip":
                versionButton.setBackgroundResource(R.drawable.version_dvd);
                break;
            case "HDTV":
                versionButton.setBackgroundResource(R.drawable.version_hdtv);
                break;
            case "HR":
                versionButton.setBackgroundResource(R.drawable.version_hr);
                break;
            case "Web-DL":
                versionButton.setBackgroundResource(R.drawable.version_wd);
                break;
        }
    }

    private void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
