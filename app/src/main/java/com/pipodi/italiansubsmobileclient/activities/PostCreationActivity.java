package com.pipodi.italiansubsmobileclient.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.TapatalkService;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.ResultMapper;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import de.timroes.axmlrpc.XMLRPCException;

public class PostCreationActivity extends AppCompatActivity {

    public static final int OK_RESULT_POST = 2;
    public static final int OK_RESULT_TOPIC = 3;
    public static final int NO_RESULT_TOPIC = 4;
    public static final int NO_RESULT_POST = 5;

    private Intent intent;
    private EditText titleBox;
    private EditText textBox;
    private String type;
    private String forumID;
    private String topicID;
    private String postTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int themeInt = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getInt("THEME", 0);
        switch (themeInt) {
            case 0:
                setTheme(R.style.AppTheme);
                break;
            case 1:
                setTheme(R.style.AppTheme_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_creation_layout);
        this.intent = getIntent();

        this.titleBox = (EditText) findViewById(R.id.postCreationTitle);
        this.textBox = (EditText) findViewById(R.id.postCreationText);
        this.intent = getIntent();
        this.type = this.intent.getStringExtra("TYPE");
        this.forumID = this.intent.getStringExtra("ID");

        if (intent.getStringExtra("QUOTE") != null) {
            this.textBox.setText(intent.getStringExtra("QUOTE"));
        }

        if (intent.getStringExtra("ID") != null && intent.getStringExtra("TOPICID") != null) {
            this.forumID = intent.getStringExtra("ID");
            this.topicID = intent.getStringExtra("TOPICID");
            this.postTitle = intent.getStringExtra("TITLE");
        }

        if (intent.getStringExtra("TYPE").equals("TOPIC")) {
            getSupportActionBar().setTitle("Creazione topic...");
        } else {
            getSupportActionBar().setTitle("Creazione post...");
            this.titleBox.setVisibility(View.GONE);
        }
        /*new AsyncTask<String, String, String>(){

            private DefaultHttpClient httpClient = new DefaultHttpClient();
            private HttpPost httpPost = null;

            @Override
            protected String doInBackground(String... strings) {
                httpPost = new HttpPost("http://www.italiansubs.net");
                httpPost.setHeader("User-Agent",
                        "ItaSA Mobile Application test by Pipodi");
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                return null;
            }
        }.execute("");*/
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.sendMenuAction:
                if (checkTextFields()) {
                    if (this.type.equals("TOPIC")) {
                        Toast.makeText(getApplicationContext(), "Invio del topic in corso...", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Invio del post in corso...", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.post_creation, menu);
        return true;
    }

    private boolean checkTextFields() {
        switch (type) {
            case "TOPIC":
                if (this.titleBox.getText().toString().equals("") || this.textBox.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Inserire titolo e/o testo del topic.", Toast.LENGTH_LONG).show();
                    return false;
                } else {
                    try {
                        final byte[] title = titleBox.getText().toString().getBytes("UTF-8");
                        final byte[] text = textBox.getText().toString().getBytes("UTF-8");
                        new AsyncTask<String, String, String>() {

                            private Boolean result;

                            @Override
                            protected String doInBackground(String... params) {
                                try {
                                    TapatalkService tapatalkService = TapatalkService.getInstance();
                                    this.result = ResultMapper.parseResult(tapatalkService.newTopic(forumID, title, text));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                } catch (XMLRPCException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                setResult(OK_RESULT_TOPIC);
                                finish();
                            }
                        }.execute("");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            case "POST":
                if (this.textBox.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Inserire il contenuto del post.", Toast.LENGTH_LONG).show();
                    return false;
                } else {
                    try {
                        final byte[] text = textBox.getText().toString().getBytes("UTF-8");
                        new AsyncTask<String, String, String>() {

                            private boolean result;

                            @Override
                            protected String doInBackground(String... params) {
                                try {
                                    TapatalkService tapatalkService = TapatalkService.getInstance();
                                    this.result = ResultMapper.parseResult(tapatalkService.newPost(forumID, topicID, postTitle.getBytes(), text));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                } catch (XMLRPCException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                setResult(OK_RESULT_POST);
                                finish();
                            }
                        }.execute("");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
        }
        return false;
    }
}
