package com.pipodi.italiansubsmobileclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.CurrentTranslationsEngine;

/**
 * Created by pipod on 27/03/2016.
 */
public class CurrentTranslationFragment extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View generalView = inflater.inflate(R.layout.current_translations_layout, container, false);
        ListView translationsList = (ListView) generalView.findViewById(R.id.showInTranslationList);
        ProgressBar progressBar = (ProgressBar) generalView.findViewById(R.id.showInTranslationLoading);
        new CurrentTranslationsEngine(translationsList, context, progressBar).execute("");
        return generalView;
    }
}
