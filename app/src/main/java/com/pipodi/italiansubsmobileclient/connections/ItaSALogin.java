package com.pipodi.italiansubsmobileclient.connections;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.activities.LoginActivity;
import com.pipodi.italiansubsmobileclient.activities.MainActivity;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.utils.Constants;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pipod on 09/04/2016.
 */
public class ItaSALogin extends AsyncTask<String, String, String> {

    private String username;
    private String password;
    private DefaultHttpClient httpClient = new DefaultHttpClient();
    private HttpPost httpPost = null;
    private boolean isFound;
    private boolean error;
    private boolean remember;
    private ViewGroup parent;
    private Button loginButton;

    public ItaSALogin(String username, String password, boolean remember, ViewGroup parent, Button loginButton) {
        this.username = username;
        this.password = password;
        this.isFound = true;
        this.remember = remember;
        this.parent = parent;
        this.loginButton = loginButton;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        LayoutInflater.from(LoginActivity.context).inflate(R.layout.login_progress_layout, parent, true);
        this.loginButton.setVisibility(View.GONE);
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            LoginVariables.getAuthCode(username, password);
        } catch (Exception e) {
            e.printStackTrace();
            this.isFound = false;
        }
        if (this.isFound) {
            try {
                httpPost = new HttpPost("https://www.italiansubs.net");
                httpPost.setHeader("User-Agent",
                        "ItaSA Mobile Application test by Pipodi");
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                List<NameValuePair> pairs = new ArrayList<>();
                Document doc = Jsoup.connect("https://www.italiansubs.net").get();
                Element loginform = doc.getElementById("form-login");
                Elements inputElements = loginform.getElementsByTag("input");
                for (Element inputElement : inputElements) {
                    String key = inputElement.attr("name");
                    String value = inputElement.attr("value");
                    if (key.equals("username"))
                        value = username;
                    else if (key.equals("passwd"))
                        value = password;
                    pairs.add(new BasicNameValuePair(key, value));
                }
                httpPost.setEntity(new UrlEncodedFormEntity(pairs));
                httpClient.execute(httpPost);
                List<Cookie> temp = httpClient.getCookieStore().getCookies();
                String cookieString = "";
                for (Cookie cookie : temp) {
                    Log.i("COOKIELOGIN", cookie.toString());
                    cookieString += cookie.getName() + "=" + cookie.getValue() + "; path=" + cookie.getPath()
                            + "; domain=" + cookie.getDomain() + "; expiry=" + cookie.getExpiryDate() + "; ";
                }
                LoginVariables.cookieString = cookieString;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                this.error = true;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                this.error = true;
            } catch (IOException e) {
                e.printStackTrace();
                this.error = true;
            }
        }
        return null;
    }


    @Override
    protected void onPostExecute(String result) {
        if (this.isFound) {
            LoginVariables.logged = true;
            SharedPreferences.Editor editor = LoginActivity.context.getSharedPreferences(Constants.SHARED_PREFS_NAME, LoginActivity.context.MODE_PRIVATE).edit();
            if (remember) {
                editor.putBoolean("REMEMBER", true).commit();
            } else {
                editor.putBoolean("REMEMBER", false).commit();
            }
            editor.putString("USER", username).commit();
            editor.putString("PASS", password).commit();
            User.getInstance().setUserName(username);
            User.getInstance().setUserPassword(password);
            Toast.makeText(LoginActivity.context, "Login effettuato con successo.",
                    Toast.LENGTH_LONG).show();
            Intent intent = new Intent(LoginActivity.context, MainActivity.class);
            LoginActivity.activity.finish();
            LoginActivity.context.startActivity(intent);
        } else if (!this.isFound && !LoginVariables.isConnected) {
            Toast.makeText(LoginActivity.context, "Problema di rete.", Toast.LENGTH_LONG).show();
            /*Intent intent = new Intent(LoginActivity.context, LoginActivity.class);
            LoginActivity.activity.finish();
            LoginActivity.context.startActivity(intent);*/
            LayoutInflater.from(LoginActivity.context).inflate(R.layout.login_activity_layout, parent, true);
        } else if (!this.isFound && LoginVariables.isConnected) {
            Toast.makeText(LoginActivity.context, "Login errato.", Toast.LENGTH_LONG).show();
            /*Intent intent = new Intent(LoginActivity.context, LoginActivity.class);
            LoginActivity.activity.finish();
            LoginActivity.context.startActivity(intent);*/
            LayoutInflater.from(LoginActivity.context).inflate(R.layout.login_activity_layout, parent, true);
        } else if (this.isFound && this.error) {
            Toast.makeText(LoginActivity.context, "Problema durante la procedura di login. Riprovare più tardi.", Toast.LENGTH_LONG).show();
            /*Intent intent = new Intent(LoginActivity.context, LoginActivity.class);
            LoginActivity.activity.finish();
            LoginActivity.context.startActivity(intent);*/
            LayoutInflater.from(LoginActivity.context).inflate(R.layout.login_activity_layout, parent, true);
        }

    }
}
