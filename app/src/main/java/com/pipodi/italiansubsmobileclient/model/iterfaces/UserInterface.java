package com.pipodi.italiansubsmobileclient.model.iterfaces;

/**
 * Created by Alex on 02/04/2016.
 */
public interface UserInterface {

    String getUserID();

    void setUserID(String userID);

    String getUserPassword();

    void setUserPassword(String userPassword);

    String getUserName();

    void setUserName(String userName);

    String getUserAvatar();

    void setUserAvatar(String userAvatar);

    String getUserPosition();

    void setUserPosition(String userPosition);

    String getUserAuthCode();

    void setUserAuthCode(String authCode);
}
