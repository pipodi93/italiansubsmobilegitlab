package com.pipodi.italiansubsmobileclient.engine;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.adapters.MultipleShowAdapter;
import com.pipodi.italiansubsmobileclient.adapters.SubtitlesListAdapter;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.Show;
import com.pipodi.italiansubsmobileclient.model.Subtitle;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ShowInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.SubtitleInterface;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 12/04/2016.
 */
public class SubtitlesParser extends AsyncTask<String, String, String> {

    private List<SubtitleInterface> items;
    private List<ShowInterface> showItems;
    private ListView listView;
    private Context context;
    private LayoutInflater inflater;
    private String name;
    private int ID;
    private String episodeAndSeason;
    private String season;
    private View parent;
    private boolean withEpisodeAndSeason;
    private boolean complete;
    private String version;
    private int page;
    private Button nextButton;
    private Button previousButton;
    private ProgressBar progressBar;

    private TextView warning;

    private boolean error = false;
    private boolean tvSeriesFound = true;
    private boolean hasNext = true;
    private int resultCount;

    public SubtitlesParser(ListView view, Context context,
                           LayoutInflater inflater, String name, int ID, String season, String episodeAndSeason, View parent, boolean choice,
                           boolean complete, String version, int page, Button nextButton, Button previousButton, ProgressBar progressBar) {
        super();
        this.listView = view;
        this.items = new ArrayList<>();
        this.context = context;
        this.inflater = inflater;
        this.name = name;
        this.ID = ID;
        this.season = season;
        this.parent = parent;
        this.withEpisodeAndSeason = choice;
        this.episodeAndSeason = episodeAndSeason;
        this.complete = complete;
        this.version = version;
        this.page = page;
        this.nextButton = nextButton;
        this.previousButton = previousButton;
        this.progressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.warning = (TextView) parent.findViewById(R.id.warningTextSub);
        this.progressBar.setVisibility(View.VISIBLE);
        if (this.warning != null) {
            this.warning.setVisibility(View.GONE);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String show_id;
        Document doc = null;
        if (!params[0].equals("")) {
            this.page = Integer.parseInt(params[0]);
        }
        try {
            doc = Connection
                    .connectToAPIURL("https://api.italiansubs.net/api/rest/shows/search?q="
                            + URLEncoder.encode(this.name, "UTF-8")
                            + "&apikey=ce0a811540244ad5267785731a5c37a0", context);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (doc == null || !LoginVariables.isRequestValid(doc)) {
            this.error = true;
        } else {
            Iterator iterator = XMLIterator.parseXMLTree(0, doc);
            Element item = (Element) iterator.next();
            if (Integer.parseInt(item.getChild("count").getText()) == 0) {
                this.tvSeriesFound = false;
                return null;
            } else {
                this.resultCount = Integer.parseInt(item.getChild("count").getText());
            }
            if (this.resultCount > 1 && this.ID == 0) {
                this.showItems = new ArrayList<>();
                iterator = XMLIterator.parseXMLTree(2, doc);
                for (int i = 0; i < this.resultCount; i++) {
                    item = (Element) iterator.next();
                    int show_id_int = Integer.parseInt(item.getChild("id").getText());
                    String show_name = item.getChild("name").getText();
                    ShowInterface show = new Show(show_name, show_id_int);
                    this.showItems.add(show);
                }
            } else {
                iterator = XMLIterator.parseXMLTree(2, doc);
                item = (Element) iterator.next();
                if (this.ID == 0) {
                    show_id = item.getChild("id").getText();
                } else {
                    show_id = "" + this.ID;
                }
                Iterator temp;
                Element tempElement;
                if (this.withEpisodeAndSeason) {
                    doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/subtitles/search?q="
                            + this.episodeAndSeason + "&show_id=" + show_id + "&apikey=" + Constants.APIKey + "&page=" + this.page, context);
                    temp = XMLIterator.parseXMLTree(0, doc);
                    tempElement = (Element) temp.next();
                    int tempInt = Integer.parseInt(tempElement.getContent(3).getValue());
                    int currentPage = Integer.parseInt(tempElement.getContent(1).getValue());
                    int totalPages = Integer.parseInt(tempElement.getContent(2).getValue());
                    if (currentPage == totalPages) {
                        this.hasNext = false;
                    }
                    if (tempInt == 0) {
                        this.tvSeriesFound = false;
                        return null;
                    }
                    temp = XMLIterator.parseXMLTree(2, doc);
                    this.items.clear();
                    while (temp.hasNext()) {
                        tempElement = (Element) temp.next();
                        String show_name = tempElement.getChild("show_name").getText();
                        if (this.version.equals("Qualsiasi")) {
                            String version = tempElement.getChild("version").getText();
                            String episode = episodeNumberParser(tempElement
                                    .getChild("name").getText(), show_name);
                            int sub_id = Integer.parseInt(tempElement.getChild("id").getText());
                            SubtitleInterface sub = new Subtitle(sub_id, show_name,
                                    episode, version);
                            this.items.add(sub);
                        } else {
                            if (tempElement.getChild("version").getText().equals(this.version)) {
                                String version = tempElement.getChild("version").getText();
                                String episode = episodeNumberParser(tempElement
                                        .getChild("name").getText(), show_name);
                                int sub_id = Integer.parseInt(tempElement.getChild("id").getText());
                                SubtitleInterface sub = new Subtitle(sub_id, show_name,
                                        episode, version);
                                this.items.add(sub);
                            }
                        }
                    }
                    temp = XMLIterator.parseXMLTree(0, doc);
                    tempElement = (Element) temp.next();
                    currentPage = Integer.parseInt(tempElement.getContent(1).getValue());
                    totalPages = Integer.parseInt(tempElement.getContent(2).getValue());
                    if (currentPage == totalPages) {
                        this.hasNext = false;
                    }
                    if (this.items.size() == 0) {
                        this.tvSeriesFound = false;
                    }
                    return null;
                } else if (complete) {
                    doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/subtitles/search?q=" + this.season + " completa&show_id=" + show_id + "&apikey=" + Constants.APIKey + "&page=" + this.page, context);
                } else {
                    try {
                        doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/subtitles/search?q="
                                + URLEncoder.encode(this.name, "UTF-8") + "&show_id=" + show_id + "&apikey=" + Constants.APIKey + "&page=" + this.page, context);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                temp = XMLIterator.parseXMLTree(0, doc);
                tempElement = (Element) temp.next();
                int tempInt = Integer.parseInt(tempElement.getContent(3).getValue());
                int currentPage = Integer.parseInt(tempElement.getContent(1).getValue());

                int totalPages = Integer.parseInt(tempElement.getContent(2).getValue());
                if (currentPage == totalPages) {
                    this.hasNext = false;
                }
                if (tempInt == 0) {
                    this.tvSeriesFound = false;
                    return null;
                }
                temp = XMLIterator.parseXMLTree(2, doc);
                while (temp.hasNext()) {
                    tempElement = (Element) temp.next();
                    String show_name = tempElement.getChild("show_name").getText();
                    String version;
                    String episode;
                    if (!season.equals("") && tempElement
                            .getChild("name").getText().contains(season + " Completa")) {
                        if (this.version.equals("Qualsiasi")) {
                            version = tempElement.getChild("version").getText();
                            episode = episodeNumberParser(tempElement
                                    .getChild("name").getText(), show_name);
                            int sub_id = Integer.parseInt(tempElement.getChild("id").getText());
                            SubtitleInterface sub = new Subtitle(sub_id, show_name,
                                    episode, version);
                            this.items.add(sub);
                        } else {
                            if (tempElement.getChild("version").getText().equals(this.version)) {
                                version = tempElement.getChild("version").getText();
                                episode = episodeNumberParser(tempElement
                                        .getChild("name").getText(), show_name);
                                int sub_id = Integer.parseInt(tempElement.getChild("id").getText());
                                SubtitleInterface sub = new Subtitle(sub_id, show_name,
                                        episode, version);
                                this.items.add(sub);
                            }
                        }
                    } else {
                        if (this.version.equals("Qualsiasi")) {
                            version = tempElement.getChild("version").getText();
                            episode = episodeNumberParser(tempElement
                                    .getChild("name").getText(), show_name);
                            int sub_id = Integer.parseInt(tempElement.getChild("id").getText());
                            SubtitleInterface sub = new Subtitle(sub_id, show_name,
                                    episode, version);
                            this.items.add(sub);
                        } else {
                            if (tempElement.getChild("version").getText().equals(this.version)) {
                                version = tempElement.getChild("version").getText();
                                episode = episodeNumberParser(tempElement
                                        .getChild("name").getText(), show_name);
                                int sub_id = Integer.parseInt(tempElement.getChild("id").getText());
                                SubtitleInterface sub = new Subtitle(sub_id, show_name,
                                        episode, version);
                                this.items.add(sub);
                            }
                        }
                    }
                }
            }
        }
        if (this.items.size() == 0 || (this.showItems != null && this.showItems.size() == 0)) {
            tvSeriesFound = false;
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        this.progressBar.setVisibility(View.GONE);
        Log.i("PostExecute", "Subs");
        if (this.error) {
            Toast.makeText(context, "Errore di rete.", Toast.LENGTH_LONG).show();
        } else if (this.resultCount == 1 || (this.resultCount > 1 && this.ID != 0)) {
            SubtitlesListAdapter adapter = new SubtitlesListAdapter(context,
                    inflater, items);
            listView.setAdapter(adapter);
            if (!tvSeriesFound) {
                if (nextButton != null && previousButton != null) {
                    nextButton.setVisibility(View.INVISIBLE);
                    previousButton.setVisibility(View.INVISIBLE);
                }
                warning.setVisibility(View.VISIBLE);
                warning.setText("Nessun risultato che soddisfi i criteri di ricerca.");
            } else {
                if (warning != null) {
                    warning.setVisibility(View.GONE);
                }
                if (!hasNext) {
                    if (nextButton != null) {
                        nextButton.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if (nextButton != null) {
                        nextButton.setVisibility(View.VISIBLE);
                    }
                }

                if (this.page > 1) {
                    if (previousButton != null) {
                        previousButton.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (previousButton != null) {
                        previousButton.setVisibility(View.INVISIBLE);
                    }
                }
            }
            if (nextButton != null) {
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TempStorage.currentSubPage++;
                        SubtitlesParser subParser = new SubtitlesParser(
                                listView, context, inflater, name, ID, season, episodeAndSeason,
                                parent, withEpisodeAndSeason, complete, version, TempStorage.currentSubPage,
                                nextButton, previousButton, progressBar);
                        subParser.execute("");
                    }
                });
            }
            if (previousButton != null) {
                previousButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (TempStorage.currentSubPage > 1) {
                            TempStorage.currentSubPage--;
                            SubtitlesParser subParser = new SubtitlesParser(
                                    listView, context, inflater, name, ID, season, episodeAndSeason,
                                    parent, withEpisodeAndSeason, complete, version, TempStorage.currentSubPage,
                                    nextButton, previousButton, progressBar);
                            subParser.execute("");
                        }

                    }
                });
            }

        } else if (this.resultCount > 1 && this.ID == 0) {
            MultipleShowAdapter multipleShowAdapter = new MultipleShowAdapter(this.showItems, this.context);
            listView.setAdapter(multipleShowAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    ShowInterface currentShow = showItems.get(i);
                    TempStorage.selectedShowID = currentShow.getShowID();
                    SubtitlesParser subtitlesParser = new SubtitlesParser(listView, context, inflater, currentShow.getShowName(), currentShow.getShowID(), season, episodeAndSeason, parent,
                            withEpisodeAndSeason, complete, version, 1, nextButton, previousButton, progressBar);
                    subtitlesParser.execute("");
                }
            });
        }
    }

    private String episodeNumberParser(String str, String name) {
        return str.replace(name + " ", "");
    }
}
