package com.pipodi.italiansubsmobileclient.utils;

/**
 * Created by Alex on 22/03/2016.
 */

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.Iterator;

public class XMLIterator {

    public static Iterator parseXMLTree(int level, Document document) {
        if (document.getContentSize() > 0) {
            Element doc = document.getRootElement();
            for (int i = 0; i < level; i++) {
                if (doc.getChildren().get(0) != null) {
                    doc = doc.getChildren().get(0);
                }
            }
            return doc.getChildren().iterator();
        }
        return null;
    }
}
