package com.pipodi.italiansubsmobileclient.model.iterfaces;

import android.graphics.Bitmap;

/**
 * Created by pipod on 22/03/2016.
 */
public interface NewsInterface {

    String getTitle();

    String getEpisode();

    String getReleaseDate();

    Bitmap getPhoto();

    String toString();

    int getID();

    int getNewsID();

    boolean isFavourite();

    void setIsFavourite(boolean isFavourite);

    String getPhotoURL();
}
