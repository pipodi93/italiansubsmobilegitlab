package com.pipodi.italiansubsmobileclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.NewsEngine;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

/**
 * Created by pipod on 22/03/2016.
 */
public class NewsFragment extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View generalView = inflater.inflate(R.layout.news_fragment_layout, container, false);
        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) generalView.findViewById(R.id.refreshLayout);
        final ListView newsListView = (ListView) generalView.findViewById(R.id.newsListView);
        newsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (newsListView == null || newsListView.getChildCount() == 0) ?
                                0 : newsListView.getChildAt(0).getTop();
                refreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

            }
        });
        if (TempStorage.downloadedNews.size() > 0) {
            new NewsEngine(context, newsListView, refreshLayout).execute("");
        } else {
            new NewsEngine(context, newsListView, refreshLayout).execute("https://api.italiansubs.net/api/rest/news?&apikey=ce0a811540244ad5267785731a5c37a0");
        }
        return generalView;
    }
}
