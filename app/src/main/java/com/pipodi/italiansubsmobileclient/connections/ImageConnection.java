package com.pipodi.italiansubsmobileclient.connections;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alex on 22/03/2016.
 */
public class ImageConnection {

    public static Bitmap getPhotoFromURL(String URL) {
        java.net.URL photo_URL;
        try {
            photo_URL = new URL(URL);
            Log.i("PhotoURL", photo_URL.toString());
            HttpURLConnection photo_connection = (HttpURLConnection) photo_URL.openConnection();
            photo_connection.setDoInput(true);
            photo_connection.connect();
            InputStream input = photo_connection.getInputStream();
            Bitmap photo = BitmapFactory.decodeStream(input);
            photo_connection.disconnect();
            return photo;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
