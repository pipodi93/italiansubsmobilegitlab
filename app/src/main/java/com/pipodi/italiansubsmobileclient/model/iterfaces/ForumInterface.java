package com.pipodi.italiansubsmobileclient.model.iterfaces;

import com.pipodi.italiansubsmobileclient.model.ForumTypes;

import java.util.Map;

/**
 * Created by Alex on 13/04/2016.
 */
public interface ForumInterface {

    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    String getForumID();

    void setForumID(String forumID);

    Map<String, Object> getChild();

    void setChild(Map<String, Object> child);

    ForumTypes getType();

    void setType(ForumTypes type);

    String toString();

    Boolean isRead();
}
