package com.pipodi.italiansubsmobileclient.model;

/**
 * Created by Alex on 13/04/2016.
 */
public enum ForumTypes {
    FORUM, TOPIC, STICKY_TOPIC, POST, UNREAD_TOPICS, UNREAD_REPLIES, OPTION
}
