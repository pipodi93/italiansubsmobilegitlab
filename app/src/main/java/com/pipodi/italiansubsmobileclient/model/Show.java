package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.ShowInterface;

/**
 * Created by pipod on 27/03/2016.
 */
public class Show implements ShowInterface {

    private String showName;
    private int showID;

    public Show(String showName, int showID) {
        this.showName = showName;
        this.showID = showID;
    }

    @Override
    public String getShowName() {
        return this.showName;
    }

    @Override
    public void setShowName(String showName) {
        this.showName = showName;
    }

    @Override
    public int getShowID() {
        return this.showID;
    }

    @Override
    public void getShowID(int ID) {
        this.showID = ID;
    }
}
