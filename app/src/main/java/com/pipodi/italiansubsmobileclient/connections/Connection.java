package com.pipodi.italiansubsmobileclient.connections;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;

/**
 * Created by Alex on 22/03/2016.
 */
public class Connection extends Thread {

    public static Document connectToAPIURL(String url, Context context) {
        try {
            Log.i("URLApi", url);
            if (checkInternetConnection(context)) {
                return new SAXBuilder().build(HTTPS.getInstance().connection(url));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }
        return false;
    }
}
