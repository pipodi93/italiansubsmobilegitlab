package com.pipodi.italiansubsmobileclient.model.iterfaces;

import android.graphics.Bitmap;

/**
 * Created by Alex on 02/04/2016.
 */
public interface ActorInterface {
    String getName();

    void setName(String name);

    String getRole();

    void setRole(String role);

    Bitmap getPhoto();
}
