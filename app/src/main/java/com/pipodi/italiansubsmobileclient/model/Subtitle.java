package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.SubtitleInterface;

/**
 * Created by Alex on 02/04/2016.
 */
public class Subtitle implements SubtitleInterface {

    private int ID;
    private String episode;
    private String name;
    private String version;

    public Subtitle(int ID, String name, String episode, String version) {
        this.ID = ID;
        this.name = name;
        this.episode = episode;
        this.version = version;
    }

    @Override
    public int getID() {
        return this.ID;
    }

    @Override
    public String getShowName() {
        return this.name;
    }

    @Override
    public String getVersion() {
        return this.version;
    }


    @Override
    public String getEpisode() {
        return this.episode;
    }

    @Override
    public String toString() {
        return "[ID:" + this.ID + " ; Name:" + this.name + " ; Episode:"
                + this.episode + " ; Version:" + this.version + "]";
    }
}
