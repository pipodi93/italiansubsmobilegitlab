package com.pipodi.italiansubsmobileclient.connections;

import android.content.Context;
import android.util.Log;

import com.pipodi.italiansubsmobileclient.activities.LoginActivity;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 02/04/2016.
 */
public class LoginVariables {

    public static String currentUsername;
    public static String currentPassword;
    public static boolean hasMYItaSA = false;
    public static boolean logged = false;
    public static boolean isConnected;
    public static String cookieString;

    private static String status = "";

    public static void getAuthCode(String username, String password) throws Exception {
        username = URLEncoder.encode(username, "UTF-8");
        password = URLEncoder.encode(password, "UTF-8");
        LoginVariables.currentPassword = password;
        LoginVariables.currentUsername = username;
        Document doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/users/login?" +
                "username=" + username + "&password=" + password + "&apikey=" + Constants.APIKey, LoginActivity.context);

        checkStatus(doc);

        if (status.equals("fail")) {
            throw new Exception();
        } else {
            Element item = (Element) XMLIterator.parseXMLTree(1, doc).next();
            User.getInstance().setUserAuthCode(item.getChild("authcode").getText());
            LoginActivity.context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit().putString("AUTHCODE", item.getChild("authcode").getText()).commit();
            if ((item.getChild("has_myitasa").getText()).equals("1")) {
                hasMYItaSA = true;
                TempStorage.favouriteShows = getFavouriteShows();
                Log.i("Serie preferite ", "" + TempStorage.favouriteShows.size());
            }
        }
    }

    public static List<String> getFavouriteShows() {
        List<String> favouriteShows = new ArrayList<>();
        Document doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/myitasa/shows?authcode="
                + User.getInstance().getUserAuthCode() + "&apikey=" + Constants.APIKey, LoginActivity.context);
        Iterator iter = XMLIterator.parseXMLTree(2, doc);
        while (iter.hasNext()) {
            Element item = (Element) iter.next();
            String show_name = item.getChild("name").getText();
            favouriteShows.add(show_name.toLowerCase());
        }
        return favouriteShows;
    }

    private static void checkStatus(Document document) {
        Iterator iterator = document.getContent().iterator();
        while (iterator.hasNext()) {
            Element item = (Element) iterator.next();
            status = item.getChild("status").getText();
        }
    }

    public static boolean isRequestValid(Document document) {
        Iterator iterator = document.getContent().iterator();
        while (iterator.hasNext()) {
            Element item = (Element) iterator.next();
            if (item.getChild("status").getText().equals("success")) {
                return true;
            }
        }
        return false;
    }
}
