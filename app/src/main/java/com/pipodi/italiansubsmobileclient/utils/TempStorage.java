package com.pipodi.italiansubsmobileclient.utils;

import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumObjInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.NewsInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ShowInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.SubtitleInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Created by Alex on 22/03/2016.
 */
public class TempStorage {

    public static List<NewsInterface> downloadedNews = new ArrayList<>();
    public static List<ShowInterface> showList = new ArrayList<>();
    public static int totalShows;
    public static int currentNewsID;
    public static int currentShowID;
    public static List<String> favouriteShows = new ArrayList<>();
    public static int currentSubPage;
    public static int selectedShowID;
    public static boolean isServiceRunning;
    public static int totalTopicsOnThisForum;
    public static int remainingTopics;
    public static boolean canUserPost;
    public static int totalPostsOnThisThread;
    public static int remainingPosts;
    public static int firstUnreadPost = 0;
    public static int firstUnreadPage = 0;
    public static int totalUnreadReplies;
    public static int totalUnreadRepliesPages;
    public static int currentUnreadRepliesPage;
    public static int endPageForum = 19;
    public static int itemToScrollTo = 0;
    public static int totalUnreadTopics;
    public static int startPageForum = 19;
    public static int startPagePost = 0;
    public static int endPagePost = 19;
    public static int totalForumPages;
    public static Stack<List<ForumObjInterface>> forumHistory = new Stack<>();
    public static Stack<String> paramsHistory = new Stack<>();
    public static Stack<Integer> totalPageHistory = new Stack<>();
    public static int currentTopicPage = 1;
    public static int currentForumPage = 1;
    public static int totalTopicPages;
    public static String[] translationForums = {"513", "515", "516", "519", "520", "528", "524",
            "523", "522", "514", "525", "526", "527", "517",
            "521", "518", "200", "201", "529"};
    public static List<String> translationList = Arrays.asList(translationForums);
    public static String[] allowedUserPositions = {"Junior", "Traduttore", "Traduttrice", "Senior", "Publisher", "Administrator"};
    public static List<String> allowedUserPositionsList = Arrays.asList(allowedUserPositions);
    public static String previousForum;
    public static int previousFourmPage;
    public static Map<String, Object> tempChild;
    public static List<ForumObjInterface> tempList = new ArrayList<>();
    public static String tempParam;
    public static List<SubtitleInterface> downloadedMyItaSA = new ArrayList<>();
}
