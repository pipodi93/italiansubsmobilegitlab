package com.pipodi.italiansubsmobileclient.engine;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.pipodi.italiansubsmobileclient.adapters.SubtitlesListAdapter;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.Subtitle;
import com.pipodi.italiansubsmobileclient.model.iterfaces.SubtitleInterface;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 02/04/2016.
 */
public class ShowSubtitlesParser extends AsyncTask<String, String, String> {

    private ProgressBar progressBar;
    private ListView listView;
    private int id;
    private String showName;
    private Context context;
    private List<SubtitleInterface> items;
    private LayoutInflater inflater;

    public ShowSubtitlesParser(Context context, String showName, int id, ProgressBar progressBar, ListView listView, LayoutInflater inflater) {
        this.progressBar = progressBar;
        this.listView = listView;
        this.id = id;
        this.showName = showName;
        this.context = context;
        this.items = new ArrayList<>();
        this.inflater = inflater;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            Document doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/subtitles/search?q=" + URLEncoder.encode(this.showName, "UTF-8") +
                    "&show_id=" + this.id + "&apikey=ce0a811540244ad5267785731a5c37a0", this.context);
            if (LoginVariables.isRequestValid(doc)) {
                Iterator iterator = XMLIterator.parseXMLTree(2, doc);
                while (iterator.hasNext()) {
                    Element item = (Element) iterator.next();
                    String version = item.getChild("version").getText();
                    String episode = episodeNumberParser(item.getChild("name").getText(), this.showName);
                    int sub_id = Integer.parseInt(item.getChild("id").getText());
                    SubtitleInterface sub = new Subtitle(sub_id, this.showName,
                            episode, version);
                    this.items.add(sub);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.progressBar.setVisibility(View.GONE);
        SubtitlesListAdapter adapter = new SubtitlesListAdapter(context,
                inflater, items);
        listView.setAdapter(adapter);
    }

    private String episodeNumberParser(String str, String name) {
        return str.replace(name + " ", "");
    }
}
