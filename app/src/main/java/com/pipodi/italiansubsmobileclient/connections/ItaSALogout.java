package com.pipodi.italiansubsmobileclient.connections;

import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.activities.LoginActivity;
import com.pipodi.italiansubsmobileclient.activities.LogoutActivity;
import com.pipodi.italiansubsmobileclient.activities.MainActivity;
import com.pipodi.italiansubsmobileclient.activities.NotificationService;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

/**
 * Created by pipod on 16/06/2016.
 */
public class ItaSALogout extends AsyncTask<String, String, String> {

    private DefaultHttpClient httpClient = new DefaultHttpClient();
    private HttpGet httpGet = null;


    public ItaSALogout() {
        super();
    }

    @Override
    protected String doInBackground(String... params) {
        httpGet = new HttpGet(
                "http://www.italiansubs.net/index.php?option=com_user&task=logout&silent=true&return=aHR0cDovL3d3dy5pdGFsaWFuc3Vicy5uZXQv");
        try {
            HttpResponse response = httpClient.execute(httpGet);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        LoginVariables.logged = false;
        LoginVariables.currentPassword = "";
        LoginVariables.currentUsername = "";
        LoginActivity.context.getSharedPreferences(Constants.SHARED_PREFS_NAME, 0).edit().clear().commit();
        TempStorage.favouriteShows = null;
        Toast.makeText(LogoutActivity.context, "Logout effettuato con successo.",
                Toast.LENGTH_LONG).show();
        LogoutActivity.activity.stopService(new Intent(LogoutActivity.context, NotificationService.class));
        TempStorage.isServiceRunning = false;
        Intent intent = new Intent(LogoutActivity.context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        LogoutActivity.context.startActivity(intent);
        MainActivity.activity.finish();
        LogoutActivity.activity.finish();
    }

}