package com.pipodi.italiansubsmobileclient.engine;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.activities.ShowInfoActivity;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.ImageConnection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

/**
 * Created by Alex on 02/04/2016.
 */
public class ShowInfoConnection extends AsyncTask<String, String, String> {

    private ViewGroup container;
    private Document document;

    private int showID;
    private ImageView banner;
    private Bitmap photo;
    private TextView plot;
    private TextView genres;
    private TextView start;
    private TextView end;
    private TextView seasons;
    private TextView country;
    private TextView network;
    private TextView runtime;
    private TextView airday;
    private TextView airtime;
    private TextView lastepnum;
    private TextView lasteptitle;
    private TextView lastepdate;
    private TextView nextepnum;
    private TextView nexteptitle;
    private TextView nextepdate;

    private String plotText;
    private String genresText;
    private String startText;
    private String endText;
    private String seasonsText;
    private String countryText;
    private String networkText;
    private String runtimeText;
    private String airdayText;
    private String airtimeText;
    private String lastepnumText;
    private String lasteptitleText;
    private String lastepdateText;
    private String nextepnumText;
    private String nexteptitleText;
    private String nextepdateText;
    private ProgressBar progressBar;

    public ShowInfoConnection(int showID, ImageView banner, TextView plot, TextView genres, TextView start,
                              TextView end, TextView seasons, TextView country, TextView network, TextView runtime,
                              TextView airday, TextView airtime, TextView lastepnum, TextView lasteptitle, TextView lastepdate,
                              TextView nextepnum, TextView nexteptitle, TextView nextepdate, ProgressBar progressBar,
                              ViewGroup container) {
        this.showID = showID;
        this.banner = banner;
        this.plot = plot;
        this.genres = genres;
        this.start = start;
        this.end = end;
        this.seasons = seasons;
        this.country = country;
        this.network = network;
        this.runtime = runtime;
        this.airday = airday;
        this.airtime = airtime;
        this.lastepnum = lastepnum;
        this.lasteptitle = lasteptitle;
        this.lastepdate = lastepdate;
        this.nextepnum = nextepnum;
        this.nexteptitle = nexteptitle;
        this.nextepdate = nextepdate;
        this.progressBar = progressBar;
        this.container = container;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.progressBar.setVisibility(View.VISIBLE);
        this.plotText = "<b>Trama: </b>";
        this.genresText = "<b>Genere: </b>";
        this.startText = "<b>Iniziata: </b>";
        this.endText = "";
        this.seasonsText = "<b>Stagioni: </b>";
        this.countryText = "<b>Paese: </b>";
        this.networkText = "<b>Emittente: </b>";
        this.runtimeText = "<b>Durata: </b>";
        this.airdayText = "<b>In onda: </b>";
        this.airtimeText = "<b>Alle ore: </b>";
        this.lastepnumText = "<b>Ultimo episodio: </b>";
        this.lasteptitleText = "<b>Titolo: </b>";
        this.lastepdateText = "<b> Andato in onda il: </b>";
        this.nextepnumText = "<b>Prossimo episodio: </b>";
        this.nexteptitleText = "<b>Titolo: </b>";
        this.nextepdateText = "<b>In onda il: </b>";
    }

    @Override
    protected String doInBackground(String... params) {
        document = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/shows/" + this.showID
                + "?apikey=" + Constants.APIKey, ShowInfoActivity.context);
        if (document == null) {
            return "";
        }
        if (!LoginVariables.isRequestValid(document)) {
            Log.i("REQUEST", "API error or invalid request.");
            return "";
        } else {
            Element item = (Element) XMLIterator.parseXMLTree(1, document).next();
            this.photo = ImageConnection.getPhotoFromURL(item.getChild("banner").getText());
            this.plotText += item.getChild("plot").getText();
            Iterator temp = item.getChild("genres").getChildren().iterator();
            int genresNum = item.getChild("genres").getChildren().size();
            int i = 0;
            while (temp.hasNext()) {
                Element element = (Element) temp.next();
                if (!element.getText().equals("")) {
                    if (i < genresNum - 1) {
                        this.genresText += element.getText() + " / ";
                    } else {
                        this.genresText += element.getText();
                    }
                }
                i++;
            }
            SimpleDateFormat formatInput = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatOutput = new SimpleDateFormat("dd/MM/yyyy");
            if (item.getChild("started") != null) {
                try {
                    this.startText += formatOutput.format(formatInput.parse(item.getChild("started").getText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (item.getChild("ended") != null && item.getChild("ended").getText().equals("0000-00-00")) {
                this.endText += "<b>Status: </b><i>in corso</i>";
            } else {
                try {
                    this.endText += "<b>Terminata: </b>" + formatOutput.format(formatInput.parse(item.getChild("ended").getText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (item.getChild("season") != null) {
                this.seasonsText += item.getChild("seasons").getText();
            }
            if (item.getChild("country") != null) {
                this.countryText += item.getChild("country").getText();
            }
            if (item.getChild("network") != null) {
                this.networkText += item.getChild("network").getText();
            }
            if (item.getChild("runtime") != null) {
                this.runtimeText += item.getChild("runtime").getText() + " minuti";
            }
            if (item.getChild("airday") != null) {
                this.airdayText += changeDayLanguage(item.getChild("airday").getText());
            }
            if (item.getChild("airtime") != null) {
                this.airtimeText += item.getChild("airtime").getText();
            }
            if (item.getChild("lastep_num") != null) {
                this.lastepnumText += item.getChild("lastep_num").getText();
            }
            if (item.getChild("lastep_title") != null) {
                this.lasteptitleText += item.getChild("lastep_title").getText();
            }
            if (item.getChild("lastep_date") != null) {
                try {
                    this.lastepdateText += formatOutput.format(formatInput.parse(item.getChild("lastep_date").getText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (item.getChild("nextep_num") != null && !item.getChild("nextep_num").getText().equals("")) {
                this.nextepnumText += item.getChild("nextep_num").getText();
            } else {
                this.nextepnumText += "-";
            }
            if (item.getChild("nextep_title") != null && !item.getChild("nextep_title").getText().equals("")) {
                this.nexteptitleText += item.getChild("nextep_title").getText();
            } else {
                this.nexteptitleText += "-";
            }
            if (item.getChild("nextep_date") != null && !item.getChild("nextep_date").getText().equals("")) {
                try {
                    this.nextepdateText += formatOutput.format(formatInput.parse(item.getChild("nextep_date").getText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                this.nextepdateText += "-";
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.progressBar.setVisibility(View.INVISIBLE);
        if (s != null && s.equals("")) {
            this.hideAll();
            Toast.makeText(ShowInfoActivity.context, "È stato riscontrato un problema durante la richiesta al server. Riprova più tardi.", Toast.LENGTH_LONG).show();
        } else {
            this.banner.setImageBitmap(photo);
            this.plot.setText(Html.fromHtml(this.plotText));
            this.genres.setText(Html.fromHtml(this.genresText));
            this.start.setText(Html.fromHtml(this.startText));
            this.end.setText(Html.fromHtml(this.endText));
            this.seasons.setText(Html.fromHtml(this.seasonsText));
            this.country.setText(Html.fromHtml(this.countryText));
            this.network.setText(Html.fromHtml(this.networkText));
            this.runtime.setText(Html.fromHtml(this.runtimeText));
            this.airday.setText(Html.fromHtml(this.airdayText));
            this.airtime.setText(Html.fromHtml(this.airtimeText));
            this.lastepnum.setText(Html.fromHtml(this.lastepnumText));
            this.lasteptitle.setText(Html.fromHtml(this.lasteptitleText));
            this.lastepdate.setText(Html.fromHtml(this.lastepdateText));
            this.nextepnum.setText(Html.fromHtml(this.nextepnumText));
            this.nexteptitle.setText(Html.fromHtml(this.nexteptitleText));
            this.nextepdate.setText(Html.fromHtml(this.nextepdateText));
        }
    }

    private String changeDayLanguage(String day) {
        switch (day) {
            case "Monday":
                return "Lunedi'";
            case "Tuesday":
                return "Martedi'";
            case "Wednesday":
                return "Mercoledi'";
            case "Thursday":
                return "Giovedi'";
            case "Friday":
                return "Venerdi'";
            case "Saturday":
                return "Sabato";
            case "Sunday":
                return "Domenica";
            default:
                return "?";
        }
    }

    private void hideAll() {
        this.banner.setImageBitmap(photo);
        this.plot.setVisibility(View.INVISIBLE);
        this.genres.setVisibility(View.INVISIBLE);
        this.start.setVisibility(View.INVISIBLE);
        this.end.setVisibility(View.INVISIBLE);
        this.seasons.setVisibility(View.INVISIBLE);
        this.country.setVisibility(View.INVISIBLE);
        this.network.setVisibility(View.INVISIBLE);
        this.runtime.setVisibility(View.INVISIBLE);
        this.airday.setVisibility(View.INVISIBLE);
        this.airtime.setVisibility(View.INVISIBLE);
        this.lastepnum.setVisibility(View.INVISIBLE);
        this.lasteptitle.setVisibility(View.INVISIBLE);
        this.lastepdate.setVisibility(View.INVISIBLE);
        this.nextepnum.setVisibility(View.INVISIBLE);
        this.nexteptitle.setVisibility(View.INVISIBLE);
        this.nextepdate.setVisibility(View.INVISIBLE);
    }
}
