package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumObjInterface;

import java.util.Map;

/**
 * Created by Alex on 13/04/2016.
 */
public class Forum implements ForumInterface, ForumObjInterface {

    private String name;
    private String description;
    private String forumID;
    private boolean read;
    private Map<String, Object> child;
    private ForumTypes type;

    public Forum(String name, String description, String forumID, Map<String, Object> child, Boolean read) {
        this.name = name;
        this.description = description;
        this.forumID = forumID;
        this.child = child;
        this.type = ForumTypes.FORUM;
        this.read = read;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getForumID() {
        return this.forumID;
    }

    @Override
    public void setForumID(String forumID) {
        this.forumID = forumID;
    }

    @Override
    public Map<String, Object> getChild() {
        return this.child;
    }

    @Override
    public void setChild(Map<String, Object> child) {
        this.child = child;
    }

    @Override
    public ForumTypes getType() {
        return this.type;
    }

    @Override
    public void setType(ForumTypes type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "[Forum: " + this.name + " | Description: " + this.description + " | ID: " + this.forumID + " ]";
    }

    @Override
    public Boolean isRead() {
        return this.read;
    }
}
