package com.pipodi.italiansubsmobileclient.adapters;

import android.content.Context;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.model.iterfaces.CurrentTranslationInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pipod on 27/03/2016.
 */
public class CurrentTranslationsAdapter extends BaseAdapter {

    private List<CurrentTranslationInterface> items = new ArrayList<>();
    private Context context;

    public CurrentTranslationsAdapter(List<CurrentTranslationInterface> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CurrentTranslationInterface showInTranslation = this.items.get(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.current_translation_item_layout, parent, false);
        TextView showName = (TextView) convertView.findViewById(R.id.showInTranslationName);
        showName.setText(showInTranslation.getName());
        ContentLoadingProgressBar progressBar = (ContentLoadingProgressBar) convertView.findViewById(R.id.showInTranslationProgress);
        progressBar.setProgress(showInTranslation.getPercent());
        TextView showPercent = (TextView) convertView.findViewById(R.id.showInTranslationPercent);
        if (showInTranslation.getPercent() == 100) {
            showPercent.setText("In Revisione");
        } else if (showInTranslation.getPercent() == 0) {
            showPercent.setText("In Traduzione");
        } else {
            showPercent.setText(showInTranslation.getPercent() + "%");
        }
        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
