package com.pipodi.italiansubsmobileclient.engine;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.pipodi.italiansubsmobileclient.adapters.CurrentTranslationsAdapter;
import com.pipodi.italiansubsmobileclient.model.CurrentTranslation;
import com.pipodi.italiansubsmobileclient.model.iterfaces.CurrentTranslationInterface;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pipod on 27/03/2016.
 */
public class CurrentTranslationsEngine extends AsyncTask<String, String, String> {

    private List<CurrentTranslationInterface> items = new ArrayList<>();
    private ListView listView;
    private Context context;
    private ProgressBar progressBar;

    public CurrentTranslationsEngine(ListView listView, Context context, ProgressBar progressBar) {
        this.listView = listView;
        this.context = context;
        this.progressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            List<String> translationsNames = new ArrayList<>();
            List<Integer> translationsProgresses = new ArrayList<>();
            Document doc = Jsoup.connect("http://www.italiansubs.net/index.php?option=com_barreavanzamento&Itemid=36").get();
            Elements links = doc.select("a");
            Elements progresses = doc.select("div");
            for (Element link : links) {
                String linkHref = link.attr("href");
                if (linkHref.contains("nomeserie")) {
                    String linkText = link.text();
                    translationsNames.add(linkText);
                }
            }
            for (Element progress : progresses) {
                Elements finalList = progress.getElementsByAttributeValue("align", "center");
                for (Element a : finalList) {
                    String percentText = a.text().replace("%", "");
                    if (percentText.contains(".")) {
                        percentText = percentText.replace(".", "");
                    }


                    if (percentText.equals("In Revisione")) {
                        translationsProgresses.add(100);
                    } else if (percentText.equals("")) {
                        translationsProgresses.add(0);
                    } else {
                        translationsProgresses.add(Integer.parseInt(percentText));
                    }
                }
            }
            for (int i = 0; i < translationsNames.size(); i++) {
                CurrentTranslationInterface showInTranslation = new CurrentTranslation(translationsNames.get(i), translationsProgresses.get(i));
                items.add(showInTranslation);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.progressBar.setVisibility(View.GONE);
        CurrentTranslationsAdapter showInTranslationAdapter = new CurrentTranslationsAdapter(items, context);
        this.listView.setAdapter(showInTranslationAdapter);
    }
}
