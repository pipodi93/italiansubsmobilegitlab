package com.pipodi.italiansubsmobileclient.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ActorInterface;

import java.util.List;

/**
 * Created by Alex on 02/04/2016.
 */
public class ActorsAdapter extends BaseAdapter {

    private Context context;
    private List<ActorInterface> items;

    public ActorsAdapter(Context context, List<ActorInterface> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public ActorInterface getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ActorInterface item = this.items.get(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.actors_list_item_layout, parent, false);
        TextView name = (TextView) convertView.findViewById(R.id.actorName);
        TextView role = (TextView) convertView.findViewById(R.id.actorAs);
        ImageView photo = (ImageView) convertView.findViewById(R.id.actorPhoto);
        String roleString = "<b>Nei panni di: </b>" + item.getRole();
        name.setText(item.getName());
        role.setText(Html.fromHtml(roleString));
        if (item.getPhoto() == null) {
            photo.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.nophoto));
        } else {
            photo.setImageBitmap(item.getPhoto());
        }
        return convertView;
    }
}
