package com.pipodi.italiansubsmobileclient.utils;

import android.util.Log;

import com.pipodi.italiansubsmobileclient.model.Forum;
import com.pipodi.italiansubsmobileclient.model.ForumOption;
import com.pipodi.italiansubsmobileclient.model.ForumTypes;
import com.pipodi.italiansubsmobileclient.model.Post;
import com.pipodi.italiansubsmobileclient.model.Topic;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumObjInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.PostInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.TopicInterface;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Alex on 13/04/2016.
 */
public class ResultMapper {

    private static List<ForumObjInterface> topicList = new ArrayList<>();

    public static List<ForumObjInterface> getResultFromArray(Object[] array) throws UnsupportedEncodingException {
        List<ForumObjInterface> forumList = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            Map<String, Object> item = (Map<String, Object>) array[i];
            Iterator<Map.Entry<String, Object>> iterator = item.entrySet().iterator();
            String name = "";
            String description = "";
            String forumID = "";
            boolean newPosts = false;
            Map<String, Object> child = new HashMap<>();
            while (iterator.hasNext()) {
                Map.Entry<String, Object> entry = iterator.next();
                switch (entry.getKey()) {
                    case "forum_name":
                        name = new String((byte[]) entry.getValue(), "UTF-8");
                        break;
                    case "description":
                        description = new String((byte[]) entry.getValue(), "UTF-8");
                        break;
                    case "forum_id":
                        forumID = (String) entry.getValue();
                        break;
                    case "child":
                        child.put(entry.getKey(), entry.getValue());
                        break;
                    case "new_post":
                        newPosts = (boolean) entry.getValue();
                        break;
                }
            }
            ForumInterface forum = new Forum(name, description, forumID, child, newPosts);
            Log.i("Added forum", forum.toString());
            forumList.add((ForumObjInterface) forum);
        }
        return forumList;
    }

    public static List<ForumObjInterface> getResultFromTopicMap(Map<String, Object> item, Boolean sticky) throws UnsupportedEncodingException {
        List<ForumObjInterface> topicList = new ArrayList<>();
        Iterator<Map.Entry<String, Object>> iterator = item.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            if (entry.getKey().equals("total_topic_num")) {
                if ((int) entry.getValue() == 0) {
                    TempStorage.totalTopicsOnThisForum = 0;
                    TempStorage.remainingTopics = 0;
                    return topicList;
                } else {
                    TempStorage.totalTopicsOnThisForum = (int) entry.getValue();
                }

            }
            if (entry.getKey().equals("topics")) {
                Object[] topics = (Object[]) entry.getValue();
                for (int i = 0; i < topics.length; i++) {
                    Map<String, Object> temp = (Map<String, Object>) topics[i];
                    Iterator<Map.Entry<String, Object>> tempIterator = temp.entrySet().iterator();
                    String title = "";
                    String topicID = "";
                    String topicAuthor = "";
                    String forumID = "";
                    ForumTypes type;
                    boolean read = false;
                    while (tempIterator.hasNext()) {
                        Map.Entry<String, Object> currentTopic = tempIterator.next();
                        switch (currentTopic.getKey()) {
                            case "topic_title":
                                title = new String((byte[]) currentTopic.getValue(), "UTF-8");
                                break;
                            case "topic_id":
                                topicID = (String) currentTopic.getValue();
                                break;
                            case "topic_author_name":
                                topicAuthor = new String((byte[]) currentTopic.getValue(), "UTF-8");
                                break;
                            case "new_post":
                                read = (boolean) currentTopic.getValue();
                                break;
                            case "forum_id":
                                forumID = (String) currentTopic.getValue();
                                break;
                        }
                    }
                    if (sticky) {
                        type = ForumTypes.STICKY_TOPIC;
                    } else {
                        type = ForumTypes.TOPIC;
                    }
                    TopicInterface topic = new Topic(title, topicID, forumID, topicAuthor, type, read, false);
                    Log.i("Added topic", topic.toString());
                    topicList.add((ForumObjInterface) topic);
                }
            } else if (entry.getKey().equals("can_post")) {
                TempStorage.canUserPost = (Boolean) entry.getValue();
            }
        }
        return topicList;
    }

    public static List<ForumObjInterface> getResultFromPostMap(Map<String, Object> item) throws UnsupportedEncodingException {
        List<ForumObjInterface> postList = new ArrayList<>();
        Iterator<Map.Entry<String, Object>> iterator = item.entrySet().iterator();
        String forumID = "";
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            if (entry.getKey().equals("forum_id")) {
                forumID = (String) entry.getValue();
            }
            if (entry.getKey().equals("total_post_num")) {
                if ((int) entry.getValue() == 0) {
                    TempStorage.totalPostsOnThisThread = 0;
                    TempStorage.remainingPosts = 0;
                    return postList;
                } else {
                    TempStorage.totalPostsOnThisThread = (int) entry.getValue();
                }
            }
            if (entry.getKey().equals("posts")) {
                Object[] posts = (Object[]) entry.getValue();
                for (int i = 0; i < posts.length; i++) {
                    Map<String, Object> temp = (Map<String, Object>) posts[i];
                    Iterator<Map.Entry<String, Object>> tempIterator = temp.entrySet().iterator();
                    String text = null;
                    String postTitle = "";
                    String postID = "";
                    String postAuthor = "";
                    String postAuthorID = "";
                    String postDate = "";
                    String topicID = "";
                    while (tempIterator.hasNext()) {
                        Map.Entry<String, Object> currentPost = tempIterator.next();
                        switch (currentPost.getKey()) {
                            case "post_title":
                                postTitle = new String((byte[]) currentPost.getValue(), "UTF-8");
                                break;
                            case "post_content":
                                text = parsePostContent(new String((byte[]) currentPost.getValue(), "UTF-8"));
                                break;
                            case "post_id":
                                postID = (String) currentPost.getValue();
                                break;
                            case "post_author_name":
                                postAuthor = new String((byte[]) currentPost.getValue(), "UTF-8");
                                break;
                            case "post_author_id":
                                postAuthorID = (String) currentPost.getValue();
                                break;
                            case "post_time":
                                Date tempDate = (Date) currentPost.getValue();
                                String outputPattern = "EEE d MMM yyyy HH:mm:ss";
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(outputPattern, Locale.ITALY);
                                postDate = simpleDateFormat.format(tempDate);
                                break;
                            case "topic_id":
                                topicID = (String) currentPost.getValue();
                                break;
                        }
                    }
                    String avatarURL = "http://www.italiansubs.net/forum/mobiquo/avatar.php?user_id=" + postAuthorID;
                    PostInterface post = new Post(forumID, topicID, postID, postTitle, text, postAuthor, postAuthorID, postDate, avatarURL);
                    Log.i("Added post", post.toString());
                    postList.add((ForumObjInterface) post);
                }
            }
        }
        return postList;
    }

    public static List<ForumObjInterface> getResultFromPostUnreadMap(Map<String, Object> item) throws UnsupportedEncodingException {
        List<ForumObjInterface> postList = new ArrayList<>();
        Iterator<Map.Entry<String, Object>> iterator = item.entrySet().iterator();
        String forumID = "";
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            if (entry.getKey().equals("forum_id")) {
                forumID = (String) entry.getValue();
            }
            if (entry.getKey().equals("total_post_num")) {
                if ((int) entry.getValue() == 0) {
                    TempStorage.totalPostsOnThisThread = 0;
                    TempStorage.remainingPosts = 0;
                    return postList;
                } else {
                    TempStorage.totalPostsOnThisThread = (int) entry.getValue();
                }
            }
            if (entry.getKey().equals("position")) {
                TempStorage.firstUnreadPost = (int) entry.getValue();
                TempStorage.firstUnreadPage = (int) Math.ceil(TempStorage.firstUnreadPost / 20.0);

            }
            if (entry.getKey().equals("posts")) {
                Object[] posts = (Object[]) entry.getValue();
                for (int i = 0; i < posts.length; i++) {
                    Map<String, Object> temp = (Map<String, Object>) posts[i];
                    Iterator<Map.Entry<String, Object>> tempIterator = temp.entrySet().iterator();
                    String text = null;
                    String postTitle = "";
                    String postID = "";
                    String postAuthor = "";
                    String postAuthorID = "";
                    String postDate = "";
                    String topicID = "";
                    while (tempIterator.hasNext()) {
                        Map.Entry<String, Object> currentPost = tempIterator.next();
                        switch (currentPost.getKey()) {
                            case "post_title":
                                postTitle = new String((byte[]) currentPost.getValue(), "UTF-8");
                                break;
                            case "post_content":
                                text = parsePostContent(new String((byte[]) currentPost.getValue(), "UTF-8"));
                                break;
                            case "post_id":
                                postID = (String) currentPost.getValue();
                                break;
                            case "post_author_name":
                                postAuthor = new String((byte[]) currentPost.getValue(), "UTF-8");
                                break;
                            case "post_author_id":
                                postAuthorID = (String) currentPost.getValue();
                                Log.i("ID", postAuthorID);
                                break;
                            case "post_time":
                                Date tempDate = (Date) currentPost.getValue();
                                String outputPattern = "EEE d MMM yyyy HH:mm:ss";
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(outputPattern, Locale.ITALY);
                                postDate = simpleDateFormat.format(tempDate);
                                break;
                            case "topic_id":
                                topicID = (String) currentPost.getValue();
                                break;
                        }
                    }
                    String avatarURL = "http://www.italiansubs.net/forum/mobiquo/avatar.php?user_id=" + postAuthorID;
                    PostInterface post = new Post(forumID, topicID, postID, postTitle, text, postAuthor, postAuthorID, postDate, avatarURL);
                    Log.i("Added post", post.toString());
                    postList.add((ForumObjInterface) post);
                }
            }
        }
        return postList;
    }


    public static List<ForumObjInterface> getResultFromPartecipatedMap(Map<String, Object> item) throws UnsupportedEncodingException {
        List<ForumObjInterface> secondList = new ArrayList<>();
        Iterator<Map.Entry<String, Object>> iterator = item.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            if (entry.getKey().equals("total_topic_num")) {
                int totUnreadReplies = (int) entry.getValue();
                if (totUnreadReplies != 0) {
                    TempStorage.totalUnreadReplies = totUnreadReplies;
                }
            }
            if (entry.getKey().equals("topics")) {
                Object[] topics = (Object[]) entry.getValue();
                for (int i = 0; i < topics.length; i++) {
                    Map<String, Object> temp = (Map<String, Object>) topics[i];
                    Iterator<Map.Entry<String, Object>> tempIterator = temp.entrySet().iterator();
                    String title = "";
                    String topicID = "";
                    String topicAuthor = "";
                    String forumID = "";
                    ForumTypes type;
                    boolean read = false;
                    while (tempIterator.hasNext()) {
                        Map.Entry<String, Object> currentTopic = tempIterator.next();
                        switch (currentTopic.getKey()) {
                            case "topic_title":
                                title = new String((byte[]) currentTopic.getValue(), "UTF-8");
                                break;
                            case "topic_id":
                                topicID = (String) currentTopic.getValue();
                                break;
                            case "post_author_name":
                                topicAuthor = new String((byte[]) currentTopic.getValue(), "UTF-8");
                                break;
                            case "new_post":
                                read = (boolean) currentTopic.getValue();
                                break;
                            case "forum_id":
                                forumID = (String) currentTopic.getValue();
                                break;
                        }
                    }
                    type = ForumTypes.UNREAD_REPLIES;
                    TopicInterface topic = new Topic(title, topicID, forumID, topicAuthor, type, read, true);
                    Log.i("TOPIC", topic.toString());
                    if (read) {
                        secondList.add((ForumObjInterface) topic);
                    }
                }
            }
        }
        if (topicList.size() > 0) {
            ForumObjInterface obj = topicList.get(topicList.size() - 1);
            if (obj.getType().equals(ForumTypes.OPTION)) {
                topicList.remove(topicList.size() - 1);
            }
        }
        topicList.addAll(secondList);
        TempStorage.totalUnreadRepliesPages = (int) Math.ceil(TempStorage.totalUnreadReplies / 20.0);
        TempStorage.currentUnreadRepliesPage = (TempStorage.endPageForum + 1) / 20;
        if (TempStorage.currentUnreadRepliesPage != TempStorage.totalUnreadRepliesPages) {
            topicList.add(new ForumOption(ForumTypes.OPTION, "Carica altro..."));
        }
        TempStorage.itemToScrollTo = topicList.size() - 1;
        /*int totalPages = (int) Math.ceil(TempStorage.totalUnreadReplies / 20.0);
        int currentPage = (TempStorage.endPageForum + 1) / 20;
        if (currentPage < totalPages){
            try {
                TempStorage.startPageForum = TempStorage.endPageForum + 1;
                TempStorage.endPageForum = TempStorage.startPageForum + 19;
                TapatalkService tapatalkService = new TapatalkService("http://www.italiansubs.net/forum/mobiquo/mobiquo.php");
                ResultMapper.getResultFromPartecipatedMap(tapatalkService.getPartecipatedTopic());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (XMLRPCException e) {
                e.printStackTrace();
            }
        }*/
        return topicList;
    }

    public static List<ForumObjInterface> getResultFromUnreadTopicMap(Map<String, Object> item) throws UnsupportedEncodingException {
        List<ForumObjInterface> topicList = new ArrayList<>();
        Iterator<Map.Entry<String, Object>> iterator = item.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            if (entry.getKey().equals("total_topic_num")) {
                int totUnreadThreads = (int) entry.getValue();
                if (totUnreadThreads != 0) {
                    TempStorage.totalUnreadTopics = totUnreadThreads;
                }
            }
            if (entry.getKey().equals("topics")) {
                Object[] topics = (Object[]) entry.getValue();
                for (int i = 0; i < topics.length; i++) {
                    Map<String, Object> temp = (Map<String, Object>) topics[i];
                    Iterator<Map.Entry<String, Object>> tempIterator = temp.entrySet().iterator();
                    String title = "";
                    String topicID = "";
                    String topicAuthor = "";
                    String forumID = "";
                    ForumTypes type;
                    boolean read = false;
                    while (tempIterator.hasNext()) {
                        Map.Entry<String, Object> currentTopic = tempIterator.next();
                        switch (currentTopic.getKey()) {
                            case "topic_title":
                                title = new String((byte[]) currentTopic.getValue(), "UTF-8");
                                break;
                            case "topic_id":
                                topicID = (String) currentTopic.getValue();
                                break;
                            case "post_author_name":
                                topicAuthor = new String((byte[]) currentTopic.getValue(), "UTF-8");
                                break;
                            case "new_post":
                                read = (boolean) currentTopic.getValue();
                                break;
                            case "forum_id":
                                forumID = (String) currentTopic.getValue();
                                break;

                        }
                    }
                    type = ForumTypes.UNREAD_TOPICS;
                    TopicInterface topic = new Topic(title, topicID, forumID, topicAuthor, type, read, true);
                    Log.i("Added topic", topic.toString());
                    topicList.add((ForumObjInterface) topic);
                }
            }
        }
        return topicList;
    }

    public static List<ForumObjInterface> parseChild(Map<String, Object> data) throws UnsupportedEncodingException {
        List<ForumObjInterface> childForums = new ArrayList<>();
        Iterator<Map.Entry<String, Object>> iterator = data.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            switch (entry.getKey()) {
                case "child":
                    Object[] childs = (Object[]) entry.getValue();
                    childForums = ResultMapper.getResultFromArray(childs);
                    break;
            }
        }
        return childForums;
    }

    public static String parsePostQuote(Map<String, Object> data) throws UnsupportedEncodingException {
        String postID = null;
        Iterator<Map.Entry<String, Object>> iterator = data.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            switch (entry.getKey()) {
                case "post_content":
                    postID = new String((byte[]) entry.getValue(), "UTF-8");
                    break;
            }
        }
        return postID;
    }

    public static boolean parseResult(Map<String, Object> data) {
        Iterator<Map.Entry<String, Object>> iterator = data.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            switch (entry.getKey()) {
                case "result":
                    if (entry.getValue().equals("true")) {
                        return true;
                    }
                    return false;
            }
        }
        return false;
    }


    public static void getUserIDAndAvatar(Map<String, Object> data) {
        Log.i("FORUM", "Getting user ID and Avatar");
        Iterator<Map.Entry<String, Object>> iterator = data.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            if (entry.getKey().equals("user_id")) {
                User.getInstance().setUserID((String) entry.getValue());

            } else if (entry.getKey().equals("icon_url")) {
                User.getInstance().setUserAvatar((String) entry.getValue());
            }
        }
    }

    private static String parsePostContent(String text) {
        String html = text;

        Map<String, String> bbMap = new HashMap<>();

        bbMap.put("(\r\n|\r|\n|\n\r)", "<br/>");
        bbMap.put("\\[b\\](.+?)\\[/b\\]", "<strong>$1</strong>");
        bbMap.put("\\[i\\](.+?)\\[/i\\]", "<span style='font-style:italic;'>$1</span>");
        bbMap.put("\\[u\\](.+?)\\[/u\\]", "<span style='text-decoration:underline;'>$1</span>");
        bbMap.put("\\[h1\\](.+?)\\[/h1\\]", "<h1>$1</h1>");
        bbMap.put("\\[h2\\](.+?)\\[/h2\\]", "<h2>$1</h2>");
        bbMap.put("\\[h3\\](.+?)\\[/h3\\]", "<h3>$1</h3>");
        bbMap.put("\\[h4\\](.+?)\\[/h4\\]", "<h4>$1</h4>");
        bbMap.put("\\[h5\\](.+?)\\[/h5\\]", "<h5>$1</h5>");
        bbMap.put("\\[h6\\](.+?)\\[/h6\\]", "<h6>$1</h6>");
        bbMap.put("\\[quote\\](.+?)\\[/quote\\]", "<span style='font-style:italic;'><blockquote>$1</blockquote></span>");
        bbMap.put("(?s)\\[quote name=\"([^\"]+)\".*\\](.+)\\[\\/quote\\]", "<span style='font-style:italic;'>Citazione di: $1 <blockquote>$2</blockquote></span>");
        bbMap.put("\\[p\\](.+?)\\[/p\\]", "<p>$1</p>");
        bbMap.put("\\[p=(.+?),(.+?)\\](.+?)\\[/p\\]", "<p style='text-indent:$1px;line-height:$2%;'>$3</p>");
        bbMap.put("\\[center\\](.+?)\\[/center\\]", "<div align='center'>$1");
        bbMap.put("\\[align=(.+?)\\](.+?)\\[/align\\]", "<div align='$1'>$2");
        bbMap.put("\\[color=(.+?)\\](.+?)\\[/color\\]", "<span style='color:$1;'>$2</span>");
        bbMap.put("\\[size=(.+?)\\](.+?)\\[/size\\]", "<span style='font-size:$1;'>$2</span>");
        bbMap.put("\\[img\\](.+?)\\[/img\\]", "<img>$1<img> ");
        bbMap.put("\\[img=(.+?),(.+?)\\](.+?)\\[/img\\]", "<img>$3<img> ");
        bbMap.put("\\[email\\](.+?)\\[/email\\]", "<a href='mailto:$1'>$1</a>");
        bbMap.put("\\[email=(.+?)\\](.+?)\\[/email\\]", "<a href='mailto:$1'>$2</a>");
        bbMap.put("\\[url\\](.+?)\\[/url\\]", "<a href='$1'>$1</a>");
        bbMap.put("\\[url=(.+?)\\](.+?)\\[/url\\]", "<a href='$1'>$2</a>");
        bbMap.put("\\[youtube\\](.+?)\\[/youtube\\]", "<object width='640' height='380'><param name='movie' value='http://www.youtube.com/v/$1'></param><embed src='http://www.youtube.com/v/$1' type='application/x-shockwave-flash' width='640' height='380'></embed></object>");
        bbMap.put("\\[video\\](.+?)\\[/video\\]", "<video src='$1' />");
        bbMap.put("(?s)\\[SPOILER\\](.+?)\\[\\/SPOILER\\]", "<spoiler>$1<spoiler>");
        bbMap.put("(?si)\\[spoiler=(.+?)\\](.+?)\\[\\/spoiler\\]", "<spoiler><name>$1<name>$2<spoiler>");

        for (Map.Entry entry : bbMap.entrySet()) {
            html = html.replaceAll(entry.getKey().toString(), entry.getValue().toString());
        }

        return html;
    }

    public static void clearTopicList() {
        topicList.clear();
    }
}