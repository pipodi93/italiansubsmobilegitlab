package com.pipodi.italiansubsmobileclient.engine;

import android.content.Intent;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.activities.NewsViewerActivity;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

/**
 * Created by Alex on 02/04/2016.
 */
public class NewsDescriptionConnection extends AsyncTask<String, String, String> {

    private String sTranslators = "";
    private String sTeam = "";
    private String sInfo = "";
    private String sResync = "";
    private String sImage = "";
    private String sReviewer = "";
    private TextView translators;
    private TextView team;
    private TextView info;
    private TextView resync;
    private TextView image;
    private TextView reviewer;
    private int newsID = 0;
    private boolean error = false;
    private String episodeString;
    private Intent intent;

    public NewsDescriptionConnection(TextView translators,
                                     TextView team, TextView info, TextView resync, TextView image,
                                     TextView reviewer, int newsID, Intent intent) {
        this.translators = translators;
        this.team = team;
        this.info = info;
        this.resync = resync;
        this.image = image;
        this.newsID = newsID;
        this.reviewer = reviewer;
        this.intent = intent;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        String temp = "";
        if (intent.getStringExtra("EPISODE") != null) {
            temp += parseEpisodes(intent.getStringExtra("EPISODE"));
        }
        NewsViewerActivity.DummySectionFragment.episodes = temp.split(" ");
    }

    @Override
    protected String doInBackground(String... params) {
        Document doc = Connection.connectToAPIURL("https://api.italiansubs.net/api/rest/news/" + this.newsID + "?apikey=ce0a811540244ad5267785731a5c37a0", NewsViewerActivity.context);
        this.sTranslators = "<b>Traduttori: </b>";
        this.sTeam = "<b>Team: </b>";
        this.sInfo = "<b>Info sulla puntata: </b>";
        this.sResync = "<b>Resync: </b>";
        this.sImage = "<b>Immagine by: </b>";
        this.sReviewer = "<b>Revisione: </b>";

        if (doc == null || !LoginVariables.isRequestValid(doc)) {
            this.error = true;
        } else {
            Element item = (Element) XMLIterator.parseXMLTree(1, doc).next();
            this.sTranslators += item.getChild("translation").getText();
            if (!item.getChild("sync").getText().equals("")) {
                this.sTeam += item.getChild("sync").getText();
            } else {
                this.sTeam += "-";
            }
            this.sInfo += item.getChild("info").getText();
            this.sReviewer += item.getChild("submitted_by").getText();
            if (!item.getChild("resync").getText().equals("")) {
                this.sResync += item.getChild("resync").getText();
            } else {
                Log.i("Resync", "nullo");
                this.sResync += "-";
            }
            if (!item.getChild("image_by").getText().equals("")) {
                this.sImage += item.getChild("image_by").getText();
            } else {
                Log.i("Immagine", "nullo");
                this.sImage += "-";
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        if (this.error) {
            this.translators.setText(Html.fromHtml(this.sTranslators) + "*ERRORE*");
            this.team.setText(Html.fromHtml(this.sTeam) + "*ERRORE*");
            this.info.setText(Html.fromHtml(this.sInfo) + "*ERRORE*");
            Toast.makeText(NewsViewerActivity.context, "Problema di rete.", Toast.LENGTH_LONG).show();

        } else {
            this.translators.setText(Html.fromHtml(this.sTranslators));
            this.team.setText(Html.fromHtml(this.sTeam));
            this.info.setText(Html.fromHtml(this.sInfo));
            this.image.setText(Html.fromHtml(this.sImage));
            this.resync.setText(Html.fromHtml(this.sResync));
            this.reviewer.setText(Html.fromHtml(this.sReviewer));
        }

    }

    private String parseEpisodes(String s) {
        return s.replace("PREAIR ", "");
    }
}
