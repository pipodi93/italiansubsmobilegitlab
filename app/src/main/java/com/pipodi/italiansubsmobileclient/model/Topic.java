package com.pipodi.italiansubsmobileclient.model;

import com.pipodi.italiansubsmobileclient.model.iterfaces.ForumObjInterface;
import com.pipodi.italiansubsmobileclient.model.iterfaces.TopicInterface;

/**
 * Created by Alex on 13/04/2016.
 */
public class Topic implements TopicInterface, ForumObjInterface {

    private String title;
    private String topicID;
    private String topicAuthor;
    private String forumID;
    private ForumTypes type;
    private boolean read;
    private boolean unreadTopicType;

    public Topic(String title, String topicID, String forumID, String topicAuthor, ForumTypes type, Boolean read, Boolean unreadTopicType) {
        this.title = title;
        this.topicID = topicID;
        this.topicAuthor = topicAuthor;
        this.forumID = forumID;
        this.type = type;
        this.read = read;
        this.unreadTopicType = unreadTopicType;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getTopicID() {
        return this.topicID;
    }

    @Override
    public String getTopicAuthor() {
        return this.topicAuthor;
    }

    @Override
    public ForumTypes getType() {
        return this.type;
    }

    @Override
    public Boolean isRead() {
        return this.read;
    }

    @Override
    public Boolean isUnreadTopicType() {
        return this.unreadTopicType;
    }

    @Override
    public String getForumID() {
        return this.forumID;
    }

    @Override
    public String toString() {
        return "[Topic: " + this.title + " | ID: " + this.topicID + " ]";
    }
}
