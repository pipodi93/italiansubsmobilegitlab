package com.pipodi.italiansubsmobileclient.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.pipodi.italiansubsmobileclient.utils.Constants;


/**
 * Created by Alex on 19/12/2015.
 */
public class ItalianSubsMobileReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isStoppedByUser = context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getBoolean("STOPPEDBYUSER", false);
        if (!isStoppedByUser) {
            context.startService(new Intent(context, NotificationService.class));
        }
    }
}
