package com.pipodi.italiansubsmobileclient.engine;

import android.util.Log;
import android.util.Pair;

import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.utils.ResultMapper;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCClient;
import de.timroes.axmlrpc.XMLRPCException;

/**
 * Created by Alex on 13/04/2016.
 */
public class TapatalkService {

    private static TapatalkService instance;

    private XMLRPCClient xmlrpcClient;
    private String username;
    private String password;


    private TapatalkService(String url) throws MalformedURLException, XMLRPCException {
        this(new URL(url));
    }

    private TapatalkService(URL url) throws XMLRPCException {
        this.setClient(new XMLRPCClient(url, XMLRPCClient.FLAGS_ENABLE_COOKIES | XMLRPCClient.FLAGS_DEFAULT_TYPE_STRING));
        this.username = User.getInstance().getUserName();
        this.password = User.getInstance().getUserPassword();
        this.loginForum(this.username, this.password);
    }

    public static TapatalkService getInstance() throws MalformedURLException, XMLRPCException {
        if (instance == null) {
            instance = new TapatalkService("https://www.italiansubs.net/forum/mobiquo/mobiquo.php");
        }
        return instance;
    }

    public Object[] getForum() throws XMLRPCException, UnsupportedEncodingException {
        //this.loginForum(this.username, this.password);
        Log.i("FORUM", "Getting forum");
        Object[] params = {};
        Object[] result = (Object[]) this.getClient().call("get_forum", params);
        return result;
    }

    public XMLRPCClient getClient() {
        return xmlrpcClient;
    }

    public void setClient(XMLRPCClient client) {
        this.xmlrpcClient = client;
    }

    private void loginForum(String loginName, String password) throws XMLRPCException {
        byte[] encodedPassword = password.getBytes();
        byte[] encodedNick = loginName.getBytes();
        Object[] params = {encodedNick, encodedPassword};
        this.getClient().setLoginData(loginName, password);
        Map<String, Object> result = (Map<String, Object>) this.getClient().call("login", params);
        Log.i("FORUM", "Login");
        ResultMapper.getUserIDAndAvatar(result);
        this.getUserPosition(getUserInfos(User.getInstance().getUserID(), this.username));
    }

    public Map<String, Object> getStickyTopic(String forumID) throws XMLRPCException {
        //this.loginForum(this.username, this.password);
        Object[] params = {forumID, 0, 3, "TOP"};
        Map<String, Object> topics = (Map<String, Object>) this.getClient().call("get_topic", params);
        return topics;
    }

    public Map<String, Object> getTopic(String forumID) throws XMLRPCException {
        //this.loginForum(this.username, this.password);
        Object[] params = {forumID, TempStorage.startPageForum, TempStorage.endPageForum};
        Map<String, Object> topics = (Map<String, Object>) this.getClient().call("get_topic", params);
        return topics;
    }

    public Map<String, Object> getPosts(String topicID) throws XMLRPCException {
        //this.loginForum(this.username, this.password);
        Object[] params = {topicID, TempStorage.startPagePost, TempStorage.endPagePost};
        Map<String, Object> result = (Map<String, Object>) this.getClient().call("get_thread", params);
        return result;
    }

    public Map<String, Object> newTopic(String forumID, byte[] subject, byte[] text) throws XMLRPCException {
        //this.loginForum(this.username, this.password);
        Object[] params = {forumID, subject, text};
        Map<String, Object> result = (Map<String, Object>) this.getClient().call("new_topic", params);
        return result;
    }

    public Map<String, Object> getQuotePost(String... postIDs) throws XMLRPCException {
        //this.loginForum(this.username, this.password);
        Object[] params;
        if (postIDs.length > 1) {
            // TODO: 03/08/2015 MultiQuote
        } else {
            params = new Object[]{postIDs[0]};
            Map<String, Object> result = (Map<String, Object>) this.getClient().call("get_quote_post", params);
            return result;
        }
        return null;
    }

    public Map<String, Object> newPost(String forumID, String topicID, byte[] title, byte[] text) throws XMLRPCException {
        //this.loginForum(this.username, this.password);
        Object[] params = {forumID, topicID, title, text};
        Map<String, Object> result = (Map<String, Object>) this.getClient().call("reply_post", params);
        return result;
    }

    public Map<String, Object> getUnreadThreads() throws XMLRPCException {
        //this.loginForum(this.username, this.password);
        Object[] params = {TempStorage.startPageForum, TempStorage.endPageForum};
        Map<String, Object> result = (Map<String, Object>) this.getClient().call("get_unread_topic", params);
        return result;
    }

    public Map<String, Object> getPartecipatedTopic() throws XMLRPCException {
        byte[] encodedNick = User.getInstance().getUserName().getBytes();
        Object[] params = {encodedNick, TempStorage.startPageForum, TempStorage.endPageForum};
        Map<String, Object> result = (Map<String, Object>) this.getClient().call("get_participated_topic", params);
        return result;
    }

    public Map<String, Object> getUserInfos(String userID, String username) throws XMLRPCException {
        //this.loginForum(this.username, this.password);
        Log.i("FORUM", "Getting user infos");
        byte[] encodedNick = this.username.getBytes();
        Object[] params = {encodedNick, userID};
        Map<String, Object> result = (Map<String, Object>) this.getClient().call("get_user_info", params);
        return result;
    }

    public Map<String, Object> getFirstUnreadPost(String topicID) throws XMLRPCException {
        Object[] params = {topicID, 20};
        Map<String, Object> result = (Map<String, Object>) this.getClient().call("get_thread_by_unread", params);
        return result;
    }

    private void getUserPosition(Map<String, Object> userInfos) {
        Log.i("FORUM", "Getting user position");
        List<Pair<String, String>> userInfosList = new ArrayList<Pair<String, String>>();
        Iterator<Map.Entry<String, Object>> iterator = userInfos.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            switch (entry.getKey()) {
                case "custom_fields_list":
                    Object[] array = (Object[]) entry.getValue();
                    for (int i = 0; i < array.length; i++) {
                        String name = "";
                        String value = "";
                        Map<String, Object> item = (Map<String, Object>) array[i];
                        Iterator<Map.Entry<String, Object>> arrayIterator = item.entrySet().iterator();
                        while (arrayIterator.hasNext()) {
                            Map.Entry<String, Object> arrayEntry = arrayIterator.next();
                            if (arrayEntry.getKey().equals("name")) {
                                try {
                                    name = new String((byte[]) arrayEntry.getValue(), "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else if (arrayEntry.getKey().equals("value")) {
                                try {
                                    value = new String((byte[]) arrayEntry.getValue(), "UTF-8");
                                    Pair userInfoPair = Pair.create(name, value);
                                    userInfosList.add(userInfoPair);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    for (Pair<String, String> pair : userInfosList) {
                        if (pair.first.equals("Posizione")) {
                            User.getInstance().setUserPosition(pair.second);
                            break;
                        }
                    }
                    break;
            }
        }
    }
}
