package com.pipodi.italiansubsmobileclient.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.engine.ForumConnection;
import com.pipodi.italiansubsmobileclient.fragments.BrowseFragment;
import com.pipodi.italiansubsmobileclient.fragments.CurrentTranslationFragment;
import com.pipodi.italiansubsmobileclient.fragments.ForumFragment;
import com.pipodi.italiansubsmobileclient.fragments.MyItaSAFragment;
import com.pipodi.italiansubsmobileclient.fragments.NewsFragment;
import com.pipodi.italiansubsmobileclient.fragments.SettingsFragment;
import com.pipodi.italiansubsmobileclient.fragments.SubtitleFragment;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.util.IabHelper;
import com.pipodi.italiansubsmobileclient.util.IabResult;
import com.pipodi.italiansubsmobileclient.util.Inventory;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static Context context;
    public static Activity activity;
    public static ForumConnection forumConnection;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FragmentManager fragmentManager;
    private Toolbar toolbar;
    private SharedPreferences preferences;
    private InputMethodManager inputMethodManager;
    private IabHelper mHelper;
    private String base64PublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlUv5l8v1lT7J/siwN9NPiItXtotuo1JGKmWw9sa" +
            "fS2wTVXkF/LuaVRWTNBVMp8a8SLKkTsSgGsd+W9FzQ6+tEtljEikmReTfoUSAJ/JKcNju+ZGkiZipLOOHK063Qrqr78o+h" +
            "mRhCyHC8WU4jyL1/GuZCYbHu8yVwVQoTA+QPyYcB/lylFwIWt3zXnNPffR1i4MV4IrSa1ROJgq5BUvT/EmBz+lJVh62cP2YHnhh" +
            "jDnxcQXmBAT7M9PD6YzgtdqfodmD/kQuPzY2ten8/oSLj1u4SqTNx1x6CJi+7udJxyqjzsaCQ2qIhln+unXviI6Ugw8Kt2+kJBeijZhU" +
            "siQYmQIDAQAB";

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int themeInt = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getInt("THEME", 0);
        switch (themeInt) {
            case 0:
                setTheme(R.style.AppTheme_NoActionBar);
                break;
            case 1:
                setTheme(R.style.AppTheme_NoActionBar_Dark);
        }
        super.onCreate(savedInstanceState);
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-9509002681827594~9426932660");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        this.fragmentManager = getSupportFragmentManager();
        context = this;
        activity = this;
        this.inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        this.preferences = this.getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE);
        final boolean changed = preferences.getBoolean("CHANGED", false);
        if (!changed) {
            this.preferences.edit().putString("DOWNLOADDIR", Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/ItaliansubsMobileSubs/").commit();
        }
        Log.i("Activity", "Return");
        this.fragmentManager.beginTransaction().replace(R.id.frameLayout, new NewsFragment()).commit();
        setContentView(R.layout.activity_main);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("News");
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Intent intentService = new Intent(this, NotificationService.class);
        boolean stoppedByUser = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getBoolean("STOPPEDBYUSER", false);
        if (!TempStorage.isServiceRunning && LoginVariables.hasMYItaSA && !stoppedByUser) {
            if (TempStorage.favouriteShows.size() > 0) {
                Log.i("Service", "Not running. Starting now.");
                startService(intentService);
            }
        } else {
            Log.i("Service", "Service already running or stopped by the user.");
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_news);
        View headerview = navigationView.getHeaderView(0);
        TextView userTextView = (TextView) headerview.findViewById(R.id.userNameTextView);
        userTextView.setText(User.getInstance().getUserName());
        mHelper = new IabHelper(this, base64PublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d("IabHelper", "Problem setting up In-app Billing: " + result);
                    if (result.getResponse() == 3) {
                        Toast.makeText(getApplicationContext(), "Servizio acquisti In-App non disponibile. " +
                                "Aggiornare Google Play Store/Google Play Services.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    IabHelper.QueryInventoryFinishedListener mGotInventoryListener
                            = new IabHelper.QueryInventoryFinishedListener() {
                        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                            if (result.isFailure()) {
                            } else {
                                boolean purchased = inventory.hasPurchase("remove_ads");
                                final AdView mAdView = (AdView) findViewById(R.id.adView);
                                if (purchased) {
                                    getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).edit().putBoolean("ADS", false).commit();
                                    mAdView.setVisibility(View.GONE);
                                } else {
                                    getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).edit().putBoolean("ADS", true).commit();
                                    mAdView.setVisibility(View.GONE);
                                    mAdView.setAdListener(new AdListener() {
                                        @Override
                                        public void onAdLoaded() {
                                            super.onAdLoaded();
                                            mAdView.setVisibility(View.VISIBLE);
                                        }
                                    });
                                    AdRequest adRequest = new AdRequest.Builder().build();
                                    mAdView.loadAd(adRequest);
                                }
                            }
                        }
                    };
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                }
            }
        });

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment forumFragment = getSupportFragmentManager().findFragmentByTag("FORUM");
            if (forumFragment != null && forumFragment.isVisible() && !TempStorage.forumHistory.isEmpty()) {
                forumConnection.backOnForum();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Accesso alla memoria esterna garantito.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_news && !item.isChecked()) {
            getSupportActionBar().setTitle("News");
            this.fragmentManager.beginTransaction().replace(R.id.frameLayout, new NewsFragment()).commit();
        } else if (id == R.id.nav_translations && !item.isChecked()) {
            getSupportActionBar().setTitle("In traduzione");
            this.fragmentManager.beginTransaction().replace(R.id.frameLayout, new CurrentTranslationFragment()).commit();
        } else if (id == R.id.nav_browse && !item.isChecked()) {
            getSupportActionBar().setTitle("Esplora");
            this.fragmentManager.beginTransaction().replace(R.id.frameLayout, new BrowseFragment()).commit();
        } else if (id == R.id.nav_subs && !item.isChecked()) {
            getSupportActionBar().setTitle("Sottotitoli");
            this.fragmentManager.beginTransaction().replace(R.id.frameLayout, new SubtitleFragment()).commit();
        } else if (id == R.id.nav_settings && !item.isChecked()) {
            getSupportActionBar().setTitle("Opzioni");
            this.fragmentManager.beginTransaction().replace(R.id.frameLayout, new SettingsFragment()).commit();
        } else if (id == R.id.nav_forum && !item.isChecked()) {
            getSupportActionBar().setTitle("Forum");
            this.fragmentManager.beginTransaction().replace(R.id.frameLayout, new ForumFragment(), "FORUM").commit();
        } else if (id == R.id.nav_myItaSA && !item.isChecked()) {
            getSupportActionBar().setTitle("MyItaSA");
            this.fragmentManager.beginTransaction().replace(R.id.frameLayout, new MyItaSAFragment()).commit();
        } else if (id == R.id.nav_logout && !item.isChecked()) {
            startActivity(new Intent(context, LogoutActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ForumConnection.REQUEST_CODE) {
            switch (resultCode) {
                case PostCreationActivity.OK_RESULT_TOPIC:
                    forumConnection.setTypeForRefresh(true);
                    break;
                case PostCreationActivity.NO_RESULT_TOPIC:
                    Toast.makeText(getApplicationContext(), "Impossibile creare il topic. Riprovare più tardi.", Toast.LENGTH_LONG).show();
                    break;
                case PostCreationActivity.OK_RESULT_POST:
                    forumConnection.setTypeForRefresh(true);
                    break;
                case PostCreationActivity.NO_RESULT_POST:
                    Toast.makeText(getApplicationContext(), "Impossibile creare il post. Riprovare più tardi.", Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
        //} else {
        //    Log.i("OnActivityResult", "Handled by IAB");
        // }
    }
}
