package com.pipodi.italiansubsmobileclient.model.iterfaces;

/**
 * Created by Alex on 02/04/2016.
 */
public interface SubtitleInterface {

    int getID();

    String getShowName();

    String getVersion();

    String getEpisode();

    String toString();
}
