package com.pipodi.italiansubsmobileclient.engine;

/**
 * Created by pipod on 27/03/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pipodi.italiansubsmobileclient.activities.ShowInfoActivity;
import com.pipodi.italiansubsmobileclient.adapters.ShowAdapter;
import com.pipodi.italiansubsmobileclient.connections.Connection;
import com.pipodi.italiansubsmobileclient.connections.LoginVariables;
import com.pipodi.italiansubsmobileclient.model.Show;
import com.pipodi.italiansubsmobileclient.model.iterfaces.ShowInterface;
import com.pipodi.italiansubsmobileclient.utils.TempStorage;
import com.pipodi.italiansubsmobileclient.utils.XMLIterator;

import org.jdom2.Document;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by alex on 07/05/15.
 */
public class BrowseShowEngine extends AsyncTask<String, String, String> {

    private List<ShowInterface> items;
    private ShowAdapter showAdapter;
    private Context context;
    private ListView listView;
    private ProgressBar progressBar;
    private boolean error;

    public BrowseShowEngine(Context context, ListView listView, ProgressBar progressBar) {
        super();
        this.context = context;
        this.listView = listView;
        this.progressBar = progressBar;
    }

    @Override
    protected String doInBackground(String... params) {
        Document doc;
        this.items = new ArrayList<>();
        if (params[0].equals("")) {
            return "";
        } else {
            doc = Connection.connectToAPIURL(params[0], context);
        }
        if (doc == null || !LoginVariables.isRequestValid(doc)) {
            this.error = true;
        } else {
            Iterator iter = XMLIterator.parseXMLTree(2, doc);
            while (iter.hasNext()) {
                Element item = (Element) iter.next();
                if (!item.getChild("name").getText().equals("")) {
                    int ID = Integer.parseInt(item.getChild("id").getText());
                    String name = item.getChild("name").getText();
                    ShowInterface show = new Show(name, ID);
                    this.items.add(show);
                }
            }
            TempStorage.showList = this.items;
            TempStorage.totalShows = this.items.size();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.progressBar.setVisibility(View.INVISIBLE);
        if (!this.error) {
            if (s != null && s.equals("")) {
                this.showAdapter = new ShowAdapter(TempStorage.showList, this.context);
            } else {
                this.showAdapter = new ShowAdapter(this.items, this.context);
            }
            this.listView.setAdapter(this.showAdapter);
            if (this.items.size() > 0) {
                this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (items.size() > 0) {
                            ShowInterface item = items.get(position);
                            Intent intent = new Intent(context, ShowInfoActivity.class);
                            intent.putExtra("TITLE", item.getShowName());
                            intent.putExtra("ID", item.getShowID());
                            context.startActivity(intent);
                        }
                    }
                });
            }
        } else {
            Toast.makeText(context, "Errore durante il download. Si prega di riprovare più tardi.", Toast.LENGTH_LONG).show();
        }
    }

    public void instantSearch(String s) {
        if (this.showAdapter != null) {
            this.showAdapter.filter(s);
        }
    }
}
