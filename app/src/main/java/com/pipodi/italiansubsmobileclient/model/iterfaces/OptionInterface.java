package com.pipodi.italiansubsmobileclient.model.iterfaces;

import com.pipodi.italiansubsmobileclient.model.OptionTypes;

/**
 * Created by Alex on 13/04/2016.
 */
public interface OptionInterface {
    String getTitle();

    void setTitle(String title);

    String getDescription();

    void setDescription(String description);

    OptionTypes getType();

    void setType(OptionTypes type);

    void enable();
}
