package com.pipodi.italiansubsmobileclient.model.iterfaces;

/**
 * Created by pipod on 27/03/2016.
 */
public interface CurrentTranslationInterface {

    String getName();

    int getPercent();
}
