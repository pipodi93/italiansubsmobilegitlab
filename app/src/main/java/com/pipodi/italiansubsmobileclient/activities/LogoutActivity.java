package com.pipodi.italiansubsmobileclient.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.connections.ItaSALogout;

public class LogoutActivity extends AppCompatActivity {

    public static Context context;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_logout);
        context = this;
        activity = this;
        ItaSALogout logout = new ItaSALogout();
        logout.execute("");
    }
}
