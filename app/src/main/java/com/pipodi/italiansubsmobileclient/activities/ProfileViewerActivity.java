package com.pipodi.italiansubsmobileclient.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.makeramen.roundedimageview.RoundedImageView;
import com.pipodi.italiansubsmobileclient.R;
import com.pipodi.italiansubsmobileclient.engine.TapatalkService;
import com.pipodi.italiansubsmobileclient.model.User;
import com.pipodi.italiansubsmobileclient.utils.Constants;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCException;

public class ProfileViewerActivity extends AppCompatActivity {

    public static Context context;
    private static TextView postCountProfile;
    private String userID;
    private String username;
    private String avatarURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int themeInt = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getInt("THEME", 0);
        switch (themeInt) {
            case 0:
                setTheme(R.style.AppTheme_Base);
                break;
            case 1:
                setTheme(R.style.AppTheme_Base_Dark);
        }
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.profile_viewer_layout);
        Intent intent = getIntent();
        if (intent.getStringExtra("USERNAME") == null) {
            this.username = User.getInstance().getUserName();
        } else {
            this.username = intent.getStringExtra("USERNAME");
        }
        this.userID = intent.getStringExtra("USERID");
        postCountProfile = (TextView) findViewById(R.id.profilePost);
        getSupportActionBar().setTitle(this.username);
        setProfile();
        boolean withAds = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getBoolean("ADS", true);
        final AdView mAdView = (AdView) findViewById(R.id.adView);
        mAdView.setVisibility(View.GONE);
        if (withAds) {
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mAdView.setVisibility(View.VISIBLE);
                }
            });
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setProfile() {
        new AsyncTask<String, String, String>() {

            private Map<String, Object> data = new HashMap<String, Object>();
            private List<Pair<String, String>> userInfos = new ArrayList<Pair<String, String>>();

            @Override
            protected String doInBackground(String... params) {
                try {
                    TapatalkService tapatalkService = TapatalkService.getInstance();
                    this.data = tapatalkService.getUserInfos(params[0], params[1]);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (XMLRPCException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Iterator<Map.Entry<String, Object>> iterator = data.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, Object> entry = iterator.next();
                    switch (entry.getKey()) {
                        case "post_count":
                            int post = (int) entry.getValue();
                            postCountProfile.setText(Html.fromHtml("<b>Post: </b>" + post));
                            break;
                        case "icon_url":
                            avatarURL = (String) entry.getValue();
                            Log.i("AVATAR", avatarURL);
                            break;
                        case "custom_fields_list":
                            Object[] array = (Object[]) entry.getValue();
                            for (int i = 0; i < array.length; i++) {
                                String name = "";
                                String value = "";
                                Map<String, Object> item = (Map<String, Object>) array[i];
                                Iterator<Map.Entry<String, Object>> arrayIterator = item.entrySet().iterator();
                                while (arrayIterator.hasNext()) {
                                    Map.Entry<String, Object> arrayEntry = arrayIterator.next();
                                    if (arrayEntry.getKey().equals("name")) {
                                        try {
                                            name = new String((byte[]) arrayEntry.getValue(), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (arrayEntry.getKey().equals("value")) {
                                        try {
                                            value = new String((byte[]) arrayEntry.getValue(), "UTF-8");
                                            Pair userInfoPair = Pair.create(name, value);
                                            userInfos.add(userInfoPair);
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
                RoundedImageView profileImg = (RoundedImageView) findViewById(R.id.profilePhoto);
                if (avatarURL != null && avatarURL.contains("http")) {
                    Picasso.with(context).load(avatarURL)
                            .fit().into(profileImg);
                } else {
                    Picasso.with(context).load(R.drawable.profile).fit().into(profileImg);
                }
                TextView profileName = (TextView) findViewById(R.id.profileName);
                profileName.setText(username);
                LinearLayout profileFields = (LinearLayout) findViewById(R.id.profileFields);
                for (Pair<String, String> pair : userInfos) {
                    TextView textView = new TextView(context);
                    textView.setText(Html.fromHtml("<b>" + pair.first + ": </b>" + pair.second));
                    profileFields.addView(textView);
                }
            }
        }.execute(this.userID, this.username);
    }
}
